# Kademlia

Code to build nodes to deploy a kademlia network

## WARNING

Due to the lack of support to **arm32** devices by google.protobuf and io.grpc, It is mandatory to do the following steps before anything else:

**This steps are only necessary on arm32 devices, the other architectures are supported by google.protobuf and io.grpc**

```bash
git clone https://github.com/Iqop/GRPConARM32 
cd GRPConARM32 
mkdir -p ~/.m2/repository/io/grpc/protoc-gen-grpc-java/1.34.0/ 
mkdir -p ~/.m2/repository/com/google/protobuf/protoc/3.14.0/ 
cp protoc-gen-grpc-java-1.34.0-linux-arm_32.exe ~/.m2/repository/io/grpc/protoc-gen-grpc-java/1.34.0/ 
cp protoc-3.14.0-linux-arm_32.exe ~/.m2/repository/com/google/protobuf/protoc/3.14.0/ 
unzip libprotoc.so.zip 
cp libprotoc.so /usr/lib/ 
unzip libprotobuf-lite.so.zip 
cp  libprotobuf-lite.so /usr/lib 
unzip libprotobuf.so.zip 
cp libprotobuf.so /usr/lib 
ldconfig 
```

This will place the necessary GRPC compiled dependencies for this project

## Dependencies

Install the following before doing anything

* openjdk-11-jdk
* maven
* redis

## Compile

Having cloned the git repository execute the following inside the **kademlia** folder

```bash
mvn clean package
```

## RUN

To run this implementation of kademlia we need to deploy two types of nodes: **BootStrapper Nodes** and **Normal Nodes**

**ipAddressToUse** should be **local** if all the nodes are in the same network, external **otherwise**

### Bootstrapper Node

Currently this implementation only supports one bootstrapper node initialized, a boostrapper node is **not** capable of storing files

The job of the bootstrapper node is to bootstrap normal nodes  into the kademlia network, otherwise the nodes would not be able to knew which nodes are in the network and how to connect them, after the bootstrap of the nodes the bootstrapper is no longer necessary

To run a bootstrapper node, run the following in the **kademlia** folder:

```bash
java -cp target/kademlia-0.1.jar inconnu.kademlia.BootStrapperNodeInit [ipAddressToUse = local|external]
```



### Normal Node

This nodes are the core of a kademlia network and are responsible to everything (FindNode,FindValue,Store and Ping).

There is *no limit* to the amount of normal nodes on this implementation of kademlia

To run a normal node, run the following in the **kademlia** folder:

```bash
java -cp target/kademlia-0.1.jar inconnu.kademlia.NormalNodeInit [boostrapperNodeIpAddress] [port] [ipAddressToUse = local|external] [allowStoreInThisNode?= true|false] [mongoDatabaseAddress]
```

**boostrapperNodeIpAddress** is the address on which the current node can reach the bootstrapper node

**port** is a local port on which the current node will be able to listen for requests

**allowStoreInThisNode?** must be **true** if the node as some storage attached to it, **false** otherwise

