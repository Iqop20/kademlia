package inconnu.kademlia;

import inconnu.kademlia.grpc.commands.SignedMessage;

import java.io.*;
import java.security.*;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;
import java.util.Arrays;

public class Utils {


    /**<pre>
     * This method is responsible to verify the signature of a SignedMessage
     * There are two signatures in a SignedMessage:
     *      - The first is the message signature, signed with the sender public key
     *      - The second is the signature of the public key performed by the bootstrapper node when the sender node entered the kademlia network
     * </pre>
     * @param signedMessage The SignedMessage to verify
     * @param nodeBootStrapperPublicKey The bootstrapper node public key
     * @return The signature verification result
     * @throws NoSuchAlgorithmException If there is no capability to deal with RSA and SHA256
     * @throws InvalidKeySpecException If theres is no capability of processing RSA public keys
     * @throws SignatureException
     * @throws InvalidKeyException If the nodeBootStrapperPublicKey renders an invalid public key
     */
    public static boolean verifySignedMessageSignatures(SignedMessage signedMessage,byte[] nodeBootStrapperPublicKey) throws NoSuchAlgorithmException, InvalidKeySpecException, SignatureException, InvalidKeyException {
        byte[] message = signedMessage.getMessage().toByteArray();
        byte[] messageSignature = signedMessage.getMessageSignature().toByteArray();
        byte[] senderPublicKeyBytes = signedMessage.getSenderPublicKeyBytes().toByteArray();
        byte[] publicKeySignatureBytes = signedMessage.getSignedPublicKeyBytes().toByteArray();

        PublicKey bootStrapperPublicKey = KeyFactory.getInstance("RSA").generatePublic(new X509EncodedKeySpec(nodeBootStrapperPublicKey));


        if (Utils.verifySignatureRSAWithSHA256(publicKeySignatureBytes,senderPublicKeyBytes,bootStrapperPublicKey)){
            PublicKey senderPublicKey = KeyFactory.getInstance("RSA").generatePublic(new X509EncodedKeySpec(senderPublicKeyBytes));
            return Utils.verifySignatureRSAWithSHA256(messageSignature,message,senderPublicKey);
        }else return false;

    }

    /**<pre>
     * This method is responsible to verify the signature of a SignedMessage
     * There are two signatures in a SignedMessage:
     *       - The first is the message signature, signed with the sender public key
     *       - The second is the signature of the public key performed by the bootstrapper node when the sender node entered the kademlia network
     * This method allows to set a message to be verified different from the message on the SignedMessage, therefore can be used to check signatures of messages
     * which content is received in chunks
     * </pre>
     * @param signedMessage The SignedMessage to verify
     * @param message The message content which the signedMessage belongs to
     * @param nodeBootStrapperPublicKey The bootstrapper node public key
     * @return The signature verification resul
     * @throws NoSuchAlgorithmException If there is no capability to deal with RSA and SHA256
     * @throws InvalidKeySpecException If theres is no capability of processing RSA public keys
     * @throws SignatureException
     * @throws InvalidKeyException If the nodeBootStrapperPublicKey renders an invalid public key
     */
    public static boolean verifySignedMessageSignatures(SignedMessage signedMessage,byte[] message,byte[] nodeBootStrapperPublicKey) throws NoSuchAlgorithmException, InvalidKeySpecException, SignatureException, InvalidKeyException {
        byte[] messageSignature = signedMessage.getMessageSignature().toByteArray();
        byte[] senderPublicKeyBytes = signedMessage.getSenderPublicKeyBytes().toByteArray();
        byte[] publicKeySignatureBytes = signedMessage.getSignedPublicKeyBytes().toByteArray();

        PublicKey bootStrapperPublicKey = KeyFactory.getInstance("RSA").generatePublic(new X509EncodedKeySpec(nodeBootStrapperPublicKey));


        if (Utils.verifySignatureRSAWithSHA256(publicKeySignatureBytes,senderPublicKeyBytes,bootStrapperPublicKey)){
            PublicKey senderPublicKey = KeyFactory.getInstance("RSA").generatePublic(new X509EncodedKeySpec(senderPublicKeyBytes));
            return Utils.verifySignatureRSAWithSHA256(messageSignature,message,senderPublicKey);
        }else return false;

    }


    /**<pre>x
     * This method generates a 4096bit RSA key pair which includes public and private keys
     * </pre>
     * @return The generated KeyPair
     * @throws NoSuchAlgorithmException If there is no capability to deal with RSA
     */
    public static KeyPair generate4096bitRSAKeyPair() throws NoSuchAlgorithmException {
        KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("RSA");
        keyPairGenerator.initialize(4096);
        return keyPairGenerator.generateKeyPair();
    }

    /**<pre>
     * This method is responsible to sign a byte array including message contents using RSA as the asymmetric encryption algorithm and SHA256 as the hash
     * </pre>
     * @param message The message to be signed
     * @param privateKey The RSA private key that will be used on the signature
     * @return The message signature result
     * @throws NoSuchAlgorithmException If there is no capability to deal with RSA and SHA256
     * @throws InvalidKeyException If the privateKey is invalid
     * @throws SignatureException
     */
    public static byte[] signMessageRSAWithSHA256(byte[] message, PrivateKey privateKey) throws NoSuchAlgorithmException, InvalidKeyException, SignatureException {
        Signature signature = Signature.getInstance("SHA256withRSA");
        signature.initSign(privateKey);
        signature.update(message);
        return signature.sign();
    }

    /**<pre>
     * This method verifies if the message signature matches the received message
     * </pre>
     * @param signedMessage The message signature, possibly obtained using Utils.signMessageRSAWithSHA256
     * @param message The message to be attested
     * @param publicKey The public key used to verify the signature
     * @return The signature verification result, true if the signature matches and false otherwise
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeyException
     * @throws SignatureException
     */
    public static boolean verifySignatureRSAWithSHA256(byte[] signedMessage,byte[] message,PublicKey publicKey) throws NoSuchAlgorithmException, InvalidKeyException, SignatureException {
        Signature signature = Signature.getInstance("SHA256withRSA");
        signature.initVerify(publicKey);
        signature.update(message);
        return signature.verify(signedMessage);
    }


    /**<pre>
     * This method generates a SHA256 hash digest from a message
     * </pre>
     * @param message The message to be hashed
     * @return The result of the message hashing
     * @throws NoSuchAlgorithmException If there is not capability to use SHA-256 algorithm
     */
    public static byte[] hashSHA256(byte[] message) throws NoSuchAlgorithmException {
        MessageDigest messageDigest =MessageDigest.getInstance("SHA-256");
        messageDigest.update(message);
        return messageDigest.digest();
    }


    /**<pre>
     * This method calculates the number of <b>consecutive</b> zero bits that the hash presents counting from the more significant bit
     * The counting stops when a bit at 1 is found and the number of zeros found until then are returned
     * It is used to check the Proof of Work used as challenge to allow the entry of nodes into the network
     * </pre>
     * @param hash The byte array to count the number of bits at zero
     * @return The number of <b>consecutive</b> zero bits found on the hash
     */
    public static int countNumberZerosBeginning(byte[] hash){
        int current_number_zeros = 0;

        boolean stop = false;

        for (int current_byte = 0; current_byte < hash.length && !stop; current_byte++) {
            byte curr = hash[current_byte];

            for (int i = 0; i < 8; i++) {
                if (getBit(curr, i) == 0) {
                    current_number_zeros++;
                } else {
                    stop = true;
                    break;
                }
            }

        }
        return current_number_zeros;

    }

    /**<pre>
     * This method get the bit at a specific position on the byte
     * </pre>
     * @param data the byte on which whe want to read a byte
     * @param pos the position that whe want to read from
     * @return the bit from data at position pos converted into integer 0 or 1
     */
    private static int getBit(byte data, int pos) {
        return (data >> (8 - (pos + 1))) & 0x0001;
    }


    /**<pre>
     * This method reads a specific amount of bytes from a input stream
     * If the specified amount is not available the method returns what is capable to read before reaching the end of the input stream
     * </pre>
     * @param inputStream The input stream to read from
     * @param amount The amount to read
     * @return A byte array containing the number of bytes read from the input stream, the length is always <= amount
     * @throws IOException If there is some problem reading from input stream
     */
    public static byte[] readBytesFromInputStream(InputStream inputStream, int amount) throws IOException {
        byte[] content = new byte[amount];
        int bytesRead;
        int alreadyRead = 0;
        while (amount != alreadyRead && (bytesRead = inputStream.read(content, alreadyRead, amount - alreadyRead)) != -1) {
            alreadyRead += bytesRead;
        }

        return Arrays.copyOfRange(content,0,alreadyRead);
    }


//    public static void writeBytesToOutputStream(byte[] input,OutputStream outputStream) throws IOException {
//        for(int i=0;i<input.length;i+=1024){
//            int lengthToSend = Math.min(input.length - i, 1024);
//            outputStream.write(input,i,lengthToSend);
//            outputStream.flush();
//        }
//    }


    /**<pre>
     * This method converts a serializable object into a byte array
     * </pre>
     * @param object The serializable object to convert into a byte array
     * @return A byte array that represents the object
     * @throws IOException If there is some problem on the convertion
     */
    public static byte[] convertObjectToByteArray(Object object) throws IOException {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ObjectOutputStream oos = new ObjectOutputStream(bos);

        oos.writeObject(object);
        oos.flush();
        oos.close();

        return bos.toByteArray();
    }

    /**<pre>
     * This method converts a byte array originated from Utils.convertObjectToByteArray into its original object
     * </pre>
     * @param bytes The byte array containing the serialized object
     * @return A Object representing the serialized object that then needs to be casted into the object type that was originally
     * @throws IOException If there is some problem reading the byte array and converting it into a Object object
     * @throws ClassNotFoundException If bytes does not represent serialized object
     */
    public static Object convertByteArrayToObject(byte[] bytes) throws IOException, ClassNotFoundException {
        ByteArrayInputStream bis = new ByteArrayInputStream(bytes);
        ObjectInputStream ois = new ObjectInputStream(bis);

        return ois.readObject();
    }

}
