package inconnu.kademlia;

public class BootStrapperNodeInit {
    public static void main(String[] args) throws Exception {
        if (args.length!=1){
            System.out.println("Needed two arguments, check README.md");
            return;
        }
        new BootStrapper(args[0]);
    }
}
