package inconnu.kademlia;

import com.google.protobuf.ByteString;
import inconnu.kademlia.grpc.boostrapperSignature.*;
import inconnu.kademlia.route.NodeID;
import io.grpc.Server;
import io.grpc.ServerBuilder;
import io.grpc.stub.StreamObserver;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.tuple.Pair;

import java.io.IOException;
import java.math.BigInteger;
import java.net.InetAddress;

import java.security.*;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;
import java.util.HashMap;

public class BootStrapper extends Node {
    public PublicKey bootStrapperPublicKey;
    public static PrivateKey bootStrapperPrivateKey;

    static HashMap<String, Pair<byte[],byte[]>> publicKeysToSign;


    /** Initializes a Kademlia Server and a Bootstrapping Server
     * The Bootstrapper node public and private key are static, the public key is of the knowledge of every node on the kademlia network
     * and it is used to attest the SignedMessages sent on the inconnu.kademlia.commands grpc's
     * @param localOrExternalIp "local" or "external", sets the ip address to use on kademlia/boostrapper
     * @throws Exception
     */
    public BootStrapper(String localOrExternalIp) throws Exception {
        super(Configuration.BOOTSTRAPPER_KADEMLIA_PORT,localOrExternalIp,"false",null);
        System.out.println(Configuration.BOOTSTRAPPER_LOG+" initializing node");

        String b64PrivateKey = "MIIJRAIBADANBgkqhkiG9w0BAQEFAASCCS4wggkqAgEAAoICAQDHVvTohAqT4DBnXPhvVM2Ryfhlg5dEVosi3irHbNspJeMamRUA1svchJvZupeY7GrPTIL6SSQUZ2GDpAVHhd+fxMC7+gxNveNDBZ0d3ctIAtguW0Ry4mCAFrnGDZhvCiveAvLB/ZaXYiUjmt94rGgsAsoOFXIgHz/aHZiulnSX/V2UaCTbXiqHPhb6RmUv8OUO1eFaWbJJ4/0hf/qJcSlDdAraZf4wLN4BevITm+FSONvjU2K2vxhCcov64T4vCizVtMYxIkoyCKzjqyh7IZBW9Bo3I2j7LWI4HSMEKpW4eVhzXvgGdemCBhARiZCz92bINAwimfJ+rXPUhfbLugni6JtJ4UNsd8Dv+EAMTlnxyLGu4BFZ6DjJdvjg4IDJYZVOz6F6nYdBSsVoA40CNASzdlWegehQ84Gi3G+o8BJUpoTuFosxsETJnQ4cfi3CDlLpnxyLG+M7el8v8zh68Y38T7DEk2deUSTcvYSJwsWoFYNk4+7ufjI3sGNpKkaeq0qjlOgnbIZRh41NC01ygIkdiuysXBzjKWkxChHVdwQbAuBrOnHmwLKIcw3omFJrMN96VhVNWLhWkV6JYF6zQ75Fq1fQYXy99KtTORtHeQ9D/Jm2AkGgcsgZSIPhSUgrxr8Qp74pB+BPKHe9UkYVi48Z99kyh8TrvWLMnNNn81aiHQIDAQABAoICAQCiBAkWGovGWMa+nA/H3PMf9ToQt/1wK672bf/zCoSuUv7clWM+xVX4K0fIlSG1xyGgzxz9dAkBFG1rYxpvYV26K15C6h6p6RYXWS60IZqhMr6mHsPpZBjuwonHcTFdvrScw2SvSv7kl358C9pp6Cba7oFvJPsXJI7vJnL8I4ksJ1+xyVt7eHO0bECcti5z4qUpg6ifmJuR9t28JpporzLMgjp3+8G3QiBGr7ilO2LNQy1k/FcVFQgQqa0gAYZbETgTkdQsBySqueZbPLewLD12jvvnqL4guGhGF1hogHwj7Wx4iM2p4pghd/ADcP3NZ92ABt1+GgW/9BZau281ckWpWMFAhKIzD4oy3OaxocdcQWEvitSUCE3m0aLydIYs89GwJoHHsLlRrWK+m0XRs+ElWZiUPIh311eANqxet1BzW/LMreh9uGkrV3ulqrCUQ7yaf21ys8EouJUSiA7pkm8elaIAWzHyA7EvtZrICknxlAEuQxOQIeWBxPEoHBXeNs9FurR3Hjovl++E7ZO9jzn4QtIFBZ7FHnWXYjeemQHeyOTJA2kFjpsTU7bTHhDpWSIsvsY0O8yOCVi+4tuO6HHOHxUZimbgI6DyPs0NFWd4Nl0gBH4VU/XY216+V7cgchepryIbFfe1JohAJSS4MRl0LO0JSruIeV1hX8V4ODU1wQKCAQEA9d/h50eIYkHguTLRVMCfQ55h7KsHex4Eb6IPQ17b96WQRi4ugJrW4Jj5KGqsmLcRG7etBPxRzninxxLgx4BrRhQSBz5byvtoJ5+jxQ32ogFf51zBgMXvB6SCQkKFXhudkqkN+rjDKHVbXZj+kjph/XOmTTJSmKqxLQ5uYed4LwHM1hzzQsDGdpI2xD76H6OfLE4lFXBPdy7nFwFhetGPtyOJsxZEl43fViNvM08YFPPHxiZrdU2VmI5miBz6lIx8Ky+s/n046bjiofb7omBSruqrbw4k2sq32xf+F720Cy8oivTT0WOtNlPt33UAvDW7BL3wKHUS/8K1rAqwHZyWeQKCAQEAz4x7s36lc9pEZWf+xZyZlCtMM3HcrFzC28mlGkArcOIMEUUq8GekLidDAV6Abh9E2mkgHHEEZ0aFEw1+imgCU/G+5xsuV5l4XvQfbzSocWsa6K9Py49uSzIL1jD12SaQWkWxPw6bNW9ivKzCSnVb2LRLGQez6Qogi5qCPZUI1qGnAvPVybOi/Ye1WmcM2j4Ika/q42uTfQpTN2vDshD/r36fpW8kNIBwBc6/sYaDjdOoe/HnxV2dY2XzIcwKipGeDKu0xcyceYeIxCFE26ZygrUdRb78DNHS9cpsmMccIf0QLAhFpoMxO5lEc0iXl/JLOUTtRH/rtfSf8keM3sPPxQKCAQEAip8HyRI+ZU9I+IEbHhJfeIsSIJGobRRWYe3Xdx9DGxNADSE6XI1vuj7kspG573u+7IaYyKfNI4BI72bIThIr6N2l8GmK7Rzi8vwBJDckJuBphpJ4EjcF9aQdQGgqsw0UwLwJQqLqfKP1uAZq+FxsJK4QQ5Osl6Actg9d0lrjGnWEW+8Eg00WV4FF2Uy8Thw8wIjqRXUFiGdsKD9CCzGdOh6SIuLaFywIj5L4vc/I/v8WUbQVf5QXVGRskC7uY9P0qZB+kxhcfwEa3T/XTQvLcgyiTMGHq8cA80hKnG361RdoXzd7OKFe5bNZ+FMJ8BLG9vxzKwwegw3uySlZc8h12QKCAQEAhc7AgbyI1uRFbEtbyfNoYEp52kDyUH2Ncz3Rn9b3KtPRi+DYCh4Hj2nonR1cF6M5NMbJ2QxUFnE0ok/04S6lMZGNzlmLyxY6sJ0bFwsP/e9bbeB4xXg3V2twjisKlXFzZ3+/AjRchFrHgXSv3mlDSbcTI5P/vaK10MtQZHuNMzMNBa5Gir+xuVqg5SPC+YE87RBJk01rzRW3COgFRuwGrBIqwvGgmeikUFo25DEg53MqEJ6pGml8avTzhzqRIT04dFrPNSbxSrjCyA/a4PGyqeoycSDsRyePpr9WvDjCPnd00a6TmQLD3vS1WM3YTjH7pXR2s871HF4WMJFIlOYRMQKCAQARsN99DPQds4XuWy7GXOdBP2YaEj6hPlgx9O4W0CJTCKrlF3ZtqZLlBonaBThloRS+ylLz0QG8kywbDZC+XBnL1JGj6THvBoe+874u0xzXdbq0N9q/ytkGryNsgHT+BvVQxBrrieYrzX/moEHWxnW8Du6stcq8nFqtPkrFoIt19o7CzXtv8wf1MDjEeuEzgbbqyyfQtE46jWUgOvdw/BjsFgMpbfSExLo/odLDoJdg0YBeKLELNCbrG4hUkHEn/w99Uf8ie/hLrP8X/Pgf9aqgjZ8qeu0W8lbjqkhwnZwquN586Mfs4sZOC/vBVUnsTde11lUGVY1j2OkBNh1pBBT0";
        String b64PublicKey = "MIICIjANBgkqhkiG9w0BAQEFAAOCAg8AMIICCgKCAgEAx1b06IQKk+AwZ1z4b1TNkcn4ZYOXRFaLIt4qx2zbKSXjGpkVANbL3ISb2bqXmOxqz0yC+kkkFGdhg6QFR4Xfn8TAu/oMTb3jQwWdHd3LSALYLltEcuJggBa5xg2Ybwor3gLywf2Wl2IlI5rfeKxoLALKDhVyIB8/2h2YrpZ0l/1dlGgk214qhz4W+kZlL/DlDtXhWlmySeP9IX/6iXEpQ3QK2mX+MCzeAXryE5vhUjjb41Nitr8YQnKL+uE+Lwos1bTGMSJKMgis46soeyGQVvQaNyNo+y1iOB0jBCqVuHlYc174BnXpggYQEYmQs/dmyDQMIpnyfq1z1IX2y7oJ4uibSeFDbHfA7/hADE5Z8cixruARWeg4yXb44OCAyWGVTs+hep2HQUrFaAONAjQEs3ZVnoHoUPOBotxvqPASVKaE7haLMbBEyZ0OHH4twg5S6Z8cixvjO3pfL/M4evGN/E+wxJNnXlEk3L2EicLFqBWDZOPu7n4yN7BjaSpGnqtKo5ToJ2yGUYeNTQtNcoCJHYrsrFwc4ylpMQoR1XcEGwLgazpx5sCyiHMN6JhSazDfelYVTVi4VpFeiWBes0O+RatX0GF8vfSrUzkbR3kPQ/yZtgJBoHLIGUiD4UlIK8a/EKe+KQfgTyh3vVJGFYuPGffZMofE671izJzTZ/NWoh0CAwEAAQ==";

        String kademliab64PrivateKey="MIIJRAIBADANBgkqhkiG9w0BAQEFAASCCS4wggkqAgEAAoICAQCra4unOlnYduczU3PWVE+1JVakuJ3c7Tt29xBbxa4Tkqd5jsotBxUhtL6hbWZsi4XT0ix6Z5nMoRuEMNL69+DUO00CdMozZJeAkRvGUA0dfcq1BZk2EgPnw1KVJMB8l42zmFUnxEnWJy3aCS+/IC5COxS/Uq9QsghdCBoJN2Ld0gfqoHuzTHHMkK9A78oG3af+bOyPHpriSjVqf5kGqChNkDQM+BOFCSQc6iKoB1u9aIqAd5793zWFl44YKtbhN57721AQx0mQcaytDb/pWtoHcfsrYXqy8LEZsUdsPD/kwddZLiju5tFdN8W1DOUb+o0iIwaSGZIEfsRav/mBqMS7rQ4n08wrWvfCv+nlE7vDBpqTx6Aoa9lsnY2JGYs5o3aD2xmEuT2iKpVCQLVYThIX5jXGfHjeLal8UC6Yt50eecvEUQrO3CsGI9lyLJTQQIjX0jXYPh3CzkHiG8wROZbdsiYtdDmOZcMTYh1f2q06szaiPQuP0ZvxyTJGncFLV4/qHCdGorLDUU5Pf/iHrgiLla1MLPRjfYrvQPC7ZhHAplfldVzH87LAwyC4jdlTZffrje6GxYSHGJ6V+3XocKL/GGw8eumrNkZA09ZoYZHFk+tSoB4ne1uxep3XIS8dq1GiJ/4c+jlA/mqNjYPf4l6chXce1TnM1YHVcjW1kvTndQIDAQABAoICAQCqWwS8umaTkwcNoP/bnopgMa07oiVwa2rPPZVKPthSp0o8qcUOnp1HlAJnFvvVH4tuCM8fDlpwXCOmaYci1rje8dgEeuB/1pYhtRPvL5r5FgwffmLWs2yiJSCrC40nxM2UD+2Aybe/VcxhhQOoY2jVdTWXNar4/RUGr4iAZESiwZoQeFC2zV/Ctn8Uf7k8vYe4d7NYQEgYXztajVgcn3z+KDMyOE/dxEnZWm36ouVvBLewGDEvh2X/T7BtIBXQWKBtF3UoV4UqQW4jxShV1m4bPBEq+YMg1caLC/04+V9eDa3f+NnBIi4q/8RBu4DtH0CnDjv3ceYFBZ5JucNSMdDgNB+1hoNmD/CWeXOLqbaNmzk1d8aThRuxsAvlU/TVpdL4eji7cuhwkAMHPGuQ6ovMUuiUXoaw1s3coQMTH1SA19uvSvtPUuXAhrN8FNR60ZgO4V4LC3/ZiWNF28qN6P0zc1xRttBVsfo+NlcMbHeqBdCGo1S1/aLjXli3AxtIPYBqfqFiVS7fLcojRKxnTCwtA4/RtOOc5NVRVeXvHacEq4jSZ5XB7mInyPY6G+dlXGuupe7KAbSvrCEJmtkzKW8vxtSN78fQyMW+r4zPuu7HWM1V1x2Hqy/YWEtxXew00V2Rlsef3XuEIP9cTYbOkEF/dnvDkye6sS1TI3GzQCYxwQKCAQEA+FYRrOylAr50vrWkVYsKMz75X/dl4JufkVwXKw9V4hepobWb43srvQjNHJ8hkDYPw4xobpmzpH9LLFs5qHVqQrNhW0VVyaD6D9qsHf92GrosYefjmlaquOm+SlH/0EE2GUg+jc7TRSJ4SutqyZFWuLDiDGXd5AjF4vmLyP8zrratF/K4ID8s0BN5rpw/Z5JJOlm/9Hltu08QJOGksRcKRkV4onuVYRZMLdFb5h5b/sbmcAi+BwVPQyBHDwS22lA5tkVxURpK6Hlkt7is6Qal9pP9Efgl4QJILtaOJKKmrdSIKyJQ0U+ljOC9dTjBvoQwuj2l/GvXqOuKTY/gllk0SQKCAQEAsLXQ5tMF8T9XKbBwlMzdIytdNZ9cV3h7UNS3fvTZ/TkoI8RoNB44etn20EqEWtZGIkfcIrks63NdwbYQchL1I7MZLBUA5b3s7iS7eXxU0Ue8+K9SpVKmX8VdzF2MeviDcPbhY9ufqMCNkSoh5K9ZWS9Urkmajij4d2rrEXAIK3/JJ2iB7/IIraPzd5I11KJC9Imy1QHh9GEwH1L/ZGrP1eXMG5ZYWGACjVi4bSSLlF+h+QvnQmW2AMY9RI4heCCIf5MgNFhU8TF2MzLXq4V2W1P1j3S1oyfdL26KlTcCuYbun7zmg5oNqiwkPNn8oaXcuFZKfl87uZHdznpVTkPBzQKCAQEA2yubDfMM7w8IGghVsOm1ZuT9UiUWVNUET1XnXKl8Mr6Ph5iwrGPYhk1uOQR4qr4Ly5nv0Sz1zwhJYCxjBroZviRymArpW7om+DcWEKOVqKEG6qGtzlkO84PX3d/g1fu3szh/fMg8zZdOc1CyPpN25im2VxrQaqEThO6Y4BvbkSbIPYqJ1mvrMPj/O4vKqa4B0u3C7BD/N4Dfzq0WnPSorLxDa6OGgtkbApTzdnT03DbOybHthGS7P5MNI8QVXTfQt30yNVHvKp14tmzWrhKgBq81etZ3F87bxfq2GL80O9aHFwqCKTkR8vOmQf6Ff49LMhOWmuDSxNKcjTeeDxNLgQKCAQBFMj0mVhXqnTIFiijbq8kgX3e1arzfJ7eqNFHSbhD6fqpAL0XausbFHJrwf22KoIOAIwGJu1dir59UHnYcJV5G53gA8gyS6idgLZQYA163QM831neXc2Dg+ck8IJnFAZF+573ku3iuvQ/yezwgb6WeLdGMGhhieJqrMHvyLRuV5Qkg3GSdT/qj/CJ3fRKFPDDZzx3vGTig2HnemkKV0Nmn2b9uQhZALQBy1v4JBxl9NWGYw1eAgZGehgIx2TIz9xFcW4Ft3fUNKaAudkdLzAxHwP3UehAyXobXbwLnYU441iSU+oSNngKh030xMu8hYXCn4mpzR9l8Vik0FGVZK2E1AoIBAQD3wrbLer5PSmaJLAoWQLS+whpImED41TwHmKtfhTU1QSn1KhBtUSP2uo685REqgikRnZ9TTw9mZlJke/B+Qgo0djWqvGQasn+NiCydr92prS0A4bHfb7fv9dgd3qVdC/wY62R+215oSAB3DxZDikyRCBUw3KnwuqCd1RSG+k/+bNMSdk5X4KYp//xaSB9nc3PQdnl2FaaQcybLV4QH0WHfQjXI3BNhXgNp5huYVFB7pKOdJ9veY+jaWKVza963Bx7c73dKWSgzFdvshlIAch44j3m4N4j3u/ilU9ZZgUXK9G/AtdWzrD0IR7fHvbvxk8+A/ICxQ4nz/Me/m0oRoORs";
        String kademliab64PublicKey="MIICIjANBgkqhkiG9w0BAQEFAAOCAg8AMIICCgKCAgEAq2uLpzpZ2HbnM1Nz1lRPtSVWpLid3O07dvcQW8WuE5KneY7KLQcVIbS+oW1mbIuF09IsemeZzKEbhDDS+vfg1DtNAnTKM2SXgJEbxlANHX3KtQWZNhID58NSlSTAfJeNs5hVJ8RJ1ict2gkvvyAuQjsUv1KvULIIXQgaCTdi3dIH6qB7s0xxzJCvQO/KBt2n/mzsjx6a4ko1an+ZBqgoTZA0DPgThQkkHOoiqAdbvWiKgHee/d81hZeOGCrW4Tee+9tQEMdJkHGsrQ2/6VraB3H7K2F6svCxGbFHbDw/5MHXWS4o7ubRXTfFtQzlG/qNIiMGkhmSBH7EWr/5gajEu60OJ9PMK1r3wr/p5RO7wwaak8egKGvZbJ2NiRmLOaN2g9sZhLk9oiqVQkC1WE4SF+Y1xnx43i2pfFAumLedHnnLxFEKztwrBiPZciyU0ECI19I12D4dws5B4hvMETmW3bImLXQ5jmXDE2IdX9qtOrM2oj0Lj9Gb8ckyRp3BS1eP6hwnRqKyw1FOT3/4h64Ii5WtTCz0Y32K70Dwu2YRwKZX5XVcx/OywMMguI3ZU2X3643uhsWEhxielft16HCi/xhsPHrpqzZGQNPWaGGRxZPrUqAeJ3tbsXqd1yEvHatRoif+HPo5QP5qjY2D3+JenIV3HtU5zNWB1XI1tZL053UCAwEAAQ==";

        bootStrapperPublicKey = KeyFactory.getInstance("RSA").generatePublic(new X509EncodedKeySpec(Base64.getDecoder().decode(b64PublicKey)));
        bootStrapperPrivateKey = KeyFactory.getInstance("RSA").generatePrivate(new PKCS8EncodedKeySpec(Base64.getDecoder().decode(b64PrivateKey)));

        PrivateKey kademliaPrivateKey = KeyFactory.getInstance("RSA").generatePrivate(new PKCS8EncodedKeySpec(Base64.getDecoder().decode(kademliab64PrivateKey)));

        super.publicKey = KeyFactory.getInstance("RSA").generatePublic(new X509EncodedKeySpec(Base64.getDecoder().decode(kademliab64PublicKey)));
        super.privateKey = kademliaPrivateKey;
        super.unique_id = new NodeID(Utils.hashSHA256(super.publicKey.getEncoded()));


        super.nodeBootStraperPublicKey = bootStrapperPublicKey.getEncoded();

        super.initRoutingTable();

        System.out.println(Configuration.BOOTSTRAPPER_LOG+" Launching Bootstrapper Server to listen from key signature requests");


        //Lauching bootstrapperSignatureGRPC
        publicKeysToSign = new HashMap<>();
        (new Thread(()->{
            try {
                Server bootstrapperServer = ServerBuilder.forPort(Configuration.BOOTSTRAPPER_BOOTSTRAP_PORT)
                        .addService(new BootStrapperSignatureService())
                        .maxInboundMessageSize(Configuration.FILE_CHUNK_LENGTH_BYTES)
                        .build()
                        .start();
                bootstrapperServer.awaitTermination();
            } catch (IOException | InterruptedException e) {
                e.printStackTrace();
            }
        })).start();


        Thread.sleep(1000);

        //Sign the kademlia component public key
        signKey(InetAddress.getLocalHost());

        //Launch kademlia server
        launchKademliaServer();
        Thread.sleep(1000);

        /*
            Initialize periodic FNODs that are responsible to keep the routing table updated
         */
        initPeriodicFNODS();


    }


    /**<pre>
     * This class extends BootStrapperSignatureGrpc.BootStrapperSignatureImplBase and implements the methods that will be responsible to answer signature and challenge requests
     * This class is then instantiated on a Grpc Server
     * </pre>
     */
     static class BootStrapperSignatureService extends BootStrapperSignatureGrpc.BootStrapperSignatureImplBase {

        /**<pre>
         * This method will answer signature requests
         * It is responsible to receive the public key to be signed, generate a request  randomId and a challenge and send it to the requester
         * It will store the public key, randomId and challenge is a list locally. That list will be accessed by the challenge method
         * </pre>
         * @param request The signature request received
         * @param responseObserver A StreamObserver that enables to answer the request received
         */
        @Override
        public void signature(SignatureRequest request, io.grpc.stub.StreamObserver<SignatureChallenge> responseObserver) {
            byte[] publicKeyBytes = request.getPublicKeyBytes().toByteArray();
            String randomId = RandomStringUtils.random(64,true,true);
            BigInteger randomChallenge = new BigInteger(512,new SecureRandom());
            publicKeysToSign.put(randomId,Pair.of(publicKeyBytes,randomChallenge.toByteArray()));

            responseObserver.onNext(SignatureChallenge.newBuilder()
                    .setRandomId(randomId)
                    .setChallenge(ByteString.copyFrom(randomChallenge.toByteArray()))
                    .setNumberZeros(Configuration.SIGN_POW_LENGTH_BITS)
                    .build());

            responseObserver.onCompleted();
        }

        /**<pre>
         * This method is responsible to check if the challenge was correctly answered
         * Attest if the requester node possesses the private key associated with the public key
         * Sign the public key if all the tests are successful and send the signature to the requester node
         * </pre>
         * @param request The challenge request received
         * @param responseObserver A StreamObserver that enables to answer the request received
         */
        @Override
        public void challenge(SignatureChallengeResponse request, StreamObserver<SignatureChallengeConfirmation> responseObserver) {
            String randomId = request.getRandomId();
            byte[] signedRandomId = request.getSignedRandomId().toByteArray();
            byte[] nonce = request.getNonce().toByteArray();

            //Get the information from the first step
            Pair<byte[], byte[]> pair = publicKeysToSign.get(randomId);
            byte[] publicKeyBytes = pair.getLeft();
            byte[] randomChallenge = pair.getRight();
            publicKeysToSign.remove(randomId);


            try {
                //Verify knowledge of the private key through signature verification of the randomId
                PublicKey publicKey = KeyFactory.getInstance("RSA").generatePublic(new X509EncodedKeySpec(publicKeyBytes));
                if (Utils.verifySignatureRSAWithSHA256(signedRandomId,randomId.getBytes(),publicKey)) {
                    //Testing nonce
                    byte[] h = new byte[randomChallenge.length + nonce.length];
                    System.arraycopy(randomChallenge, 0, h, 0, randomChallenge.length);
                    System.arraycopy(nonce, 0, h, randomChallenge.length, nonce.length);

                    if (Utils.countNumberZerosBeginning(Utils.hashSHA256(h)) >= Configuration.SIGN_POW_LENGTH_BITS) {
                        //Nonce is valid, sending the signed public key
                        byte[] signedPublicKey = Utils.signMessageRSAWithSHA256(publicKeyBytes, bootStrapperPrivateKey);
                        responseObserver.onNext(SignatureChallengeConfirmation.newBuilder().setPublicKeyBytesSigned(ByteString.copyFrom(signedPublicKey)).build());

                    } else {
                        responseObserver.onError(new Exception("PKey verification failed"));
                    }
                    //Mark transmission as complete
                    responseObserver.onCompleted();
                }
            } catch (InvalidKeySpecException | NoSuchAlgorithmException | InvalidKeyException | SignatureException e) {
                e.printStackTrace();
                responseObserver.onError(e);
            }


        }
    }

}
