package inconnu.kademlia;

import com.google.protobuf.ByteString;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.Filters;
import inconnu.kademlia.commands.*;
import inconnu.kademlia.grpc.boostrapperSignature.*;
import inconnu.kademlia.route.NodeID;
import inconnu.kademlia.route.RoutingTable;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.Server;
import io.grpc.ServerBuilder;
import org.apache.commons.lang3.RandomStringUtils;
import org.bson.Document;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPubSub;

import java.io.*;
import java.math.BigInteger;
import java.net.InetAddress;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.*;
import java.util.*;
import java.util.concurrent.*;

public class Node {
    public byte[] nodeBootStraperPublicKey;
    public PublicKey publicKey;
    public PrivateKey privateKey;
    public byte[] signedKey;
    public NodeID unique_id;
    public RoutingTable routingTable;
    public boolean allowStore;
    public int port;
    public String ipAddress;

    /**<pre>
     * This method is responsible to initialize a Kademlia Node
     * Generate a public and private key pair and generate a unique id (SHA256 of the public key)
     * Resolve the ipAddress based  localOrExternalIp argument
     * </pre>
     * @param port The port that the kademlia server will be listening to
     * @param localOrExternalIp "local" or "external", sets the ip address to use on kademlia
     * @param store "true" or "false" and means if this node is capable of storing data
     * @throws Exception
     */
    public Node(int port,String localOrExternalIp,String store,String mongoAddress) throws Exception {
        System.out.println("Number threads for store/retrieve is "+Configuration.STORE_FILE_MAX_THREADS);
        System.out.println(Configuration.KADEMLIA_LOG+" Generating Kademlia Node public/private key pair");
        KeyPair kp = Utils.generate4096bitRSAKeyPair();
        this.publicKey = kp.getPublic();
        this.privateKey = kp.getPrivate();
        this.unique_id = new NodeID(Utils.hashSHA256(publicKey.getEncoded()));
        System.out.println(Configuration.KADEMLIA_LOG+" Unique ID is -> "+this.unique_id);
        this.port = port;

        allowStore= store.equals("true");


        if (allowStore && mongoAddress!=null){
            new Thread(()-> {
                Jedis jedis = new Jedis();
                jedis.configSet("notify-keyspace-events", "KEAx");
                jedis.psubscribe(new JedisPubSub() {
                    @Override
                    public void onPMessage(String pattern, String channel, String key) {
                        super.onPMessage(pattern, channel, key);
                        if (channel.contains(":expired")) {
                            MongoClient mongoClient = new MongoClient(mongoAddress);
                            try {
                                MongoCollection<Document> collection = mongoClient.getDatabase("inconnu").getCollection("kademlia");
                                Document found = collection.find(Filters.in("chunks", key)).first();
                                if (found != null) {
                                    jedis.set(key, key);
                                    jedis.expire(key, Configuration.STORE_ENTRY_EXPIRICY);
                                } else {
                                    if (Files.exists(Paths.get(Configuration.STORE_PATH + key))) {
                                        try {
                                            Files.delete(Paths.get(Configuration.STORE_PATH + key));
                                        } catch (IOException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                }
                            }catch (Exception e){
                                e.printStackTrace();
                                jedis.set(key, key);
                                jedis.expire(key, Configuration.STORE_ENTRY_EXPIRICY);
                            }
                        }
                    }
                }, "*");
            }).start();
        }



        //Resolve the ipAddress to be used
        if (localOrExternalIp.equals("external")) {
            URL whatismyip = new URL("http://checkip.amazonaws.com");
            BufferedReader in = new BufferedReader(new InputStreamReader(
                    whatismyip.openStream()));

            this.ipAddress = in.readLine();
        }else if (localOrExternalIp.equals("local")){
            this.ipAddress =InetAddress.getLocalHost().getHostAddress();
        }else{
            throw new Exception("localOrExternalIp argument badly defined");
        }
    }


    /**<pre>
     * This method makes signature and challenge requests to the BootStrapper node to attempt to receive the public key signed
     * </pre>
     * @param address BootStrapper node ip address
     * @throws Exception
     */

    void signKey(InetAddress address) throws Exception {
        System.out.println(Configuration.KADEMLIA_LOG+" Attempting to sign the node public key on the bootstrapper node located at -> "+address);

        //Opening Channel
        ManagedChannel channel = ManagedChannelBuilder.forAddress(address.getHostAddress(), Configuration.BOOTSTRAPPER_BOOTSTRAP_PORT)
                .usePlaintext()
                .maxInboundMessageSize(Configuration.FILE_CHUNK_LENGTH_BYTES)
                .build();

        //Calling stub
        BootStrapperSignatureGrpc.BootStrapperSignatureBlockingStub bootStrapperSignatureBlockingStub = BootStrapperSignatureGrpc.newBlockingStub(channel);


        SignatureChallenge signatureChallenge = bootStrapperSignatureBlockingStub.signature(SignatureRequest.newBuilder().setPublicKeyBytes(ByteString.copyFrom(publicKey.getEncoded())).build());

        //Get challenge
        byte[] challenge = signatureChallenge.getChallenge().toByteArray();
        String randomId = signatureChallenge.getRandomId();
        int numberZerosPOW = signatureChallenge.getNumberZeros();


        int nonce = Integer.MIN_VALUE;

        boolean found =false;


        byte[] test = new byte[challenge.length+Integer.BYTES];
        while(!found && nonce<Integer.MAX_VALUE){

            System.arraycopy(challenge,0,test,0,challenge.length);
            System.arraycopy(BigInteger.valueOf(nonce).toByteArray(),0,test,challenge.length,Integer.BYTES);

            if (Utils.countNumberZerosBeginning(Utils.hashSHA256(test))>=numberZerosPOW){
                found=true;
            }else nonce++;
        }


        SignatureChallengeResponse signatureChallengeResponse;
        if (found) {
            signatureChallengeResponse = SignatureChallengeResponse.newBuilder()
                    .setNonce(ByteString.copyFrom(BigInteger.valueOf(nonce).toByteArray()))
                    .setRandomId(randomId)
                    .setSignedRandomId(ByteString.copyFrom(Utils.signMessageRSAWithSHA256(randomId.getBytes(),privateKey)))
                    .build();
        }else{
            //Send to force the bootstrapper server to clear the cached data from the system
            signatureChallengeResponse = SignatureChallengeResponse.newBuilder().setRandomId(randomId).build();
        }

        //Send the nonce
        SignatureChallengeConfirmation challengeConfirmation = bootStrapperSignatureBlockingStub.challenge(signatureChallengeResponse);

        ByteString publicKeyBytesSigned = challengeConfirmation.getPublicKeyBytesSigned();


        //Get the signed public key
        signedKey = publicKeyBytesSigned.toByteArray();

        channel.shutdownNow();
        while(!channel.awaitTermination(1,TimeUnit.MINUTES)){
            channel.shutdownNow();
        }
        System.out.println(Configuration.KADEMLIA_LOG+" Key signature performed");
    }

    /**
     * <pre>
     * Instantiates the routing table
     * </pre>
     */
    void initRoutingTable(){
        System.out.println(Configuration.KADEMLIA_LOG+" Initializing routing table");
        routingTable = new RoutingTable(this);
    }


    /**
     * <pre>
     * Launches a kademlia server on a new thread
     * </pre>
     */
    void launchKademliaServer(){
        System.out.println(Configuration.KADEMLIA_LOG+" Launching Kademlia server");
        (new Thread(()->{
            try {
                Server kademliaServer = ServerBuilder.forPort(port)
                        .addService(new Ping.PingService(this))
                        .addService(new Store.StoreService(this))
                        .addService(new FindValue.FindValueService(this))
                        .addService(new FindNode.FindNodeService(this))
                        .maxInboundMessageSize(Configuration.FILE_CHUNK_LENGTH_BYTES)
                        .build()
                        .start();
                kademliaServer.awaitTermination();
            } catch (IOException | InterruptedException e) {
                e.printStackTrace();
            }

        })).start();

    }

    /**
     * <pre>
     *     Starts a thread a periodically will launch a FNOD rpc request to update the routing table
     * </pre>
     */
    void initPeriodicFNODS() {
        System.out.println(Configuration.KADEMLIA_LOG+" Performing FNOD command on the available routing table routes every ["+Configuration.PERIODIC_FNOD_MINIMAL_WAIT+", "+Configuration.PERIODIC_FNOD_MAXIMUM_WAIT+"]s");
        (new Thread(new PeriodicFNOD(this))).start();
    }

    void initPeriodicReuploader(){
        System.out.println(Configuration.KADEMLIA_LOG+" Initializing Periodic Reuploader ["+Configuration.PERIODIC_REUPLOADER_MINIMAL_WAIT+", "+Configuration.PERIODIC_REUPLOADER_MAXIMUM_WAIT+"]s");
        (new Thread(new PeriodicReuploader(this))).start();
    }






    /**<pre>
     * This method will read the data from the input stream chunk by chunk and for each chunk call a store request rpc
     * In order to improve store speeds the store requests are parallelized using  Configuration.STORE_FILE_MAX_THREADS number of threads
     * The time that this method take to finish will be printed on the console containing  "... Store lasted ..."
     * </pre>
     * @param input A InputStream containing the data to store
     * @return A List of keys of all the stored chunks of the data to store, each key is in Base64
     * @throws IOException
     * @throws NoSuchAlgorithmException
     * @throws InterruptedException
     */
    public List<String> store(InputStream input) throws IOException, NoSuchAlgorithmException, InterruptedException {

        long init = System.currentTimeMillis();

        Thread[] threadPool = new Thread[Configuration.STORE_FILE_MAX_THREADS];


        byte[] current = Utils.readBytesFromInputStream(input, Configuration.FILE_CHUNK_LENGTH_BYTES);


        List<String> hashList = new LinkedList<>();


        int current_chunk_number=0;
        //Read until there is no more data
        while (current.length!=0){

            //Calculate hash
            byte[] hashValue = Utils.hashSHA256(current);


            //Load request into the pool
            if (threadPool[current_chunk_number%Configuration.STORE_FILE_MAX_THREADS]!=null){
                threadPool[current_chunk_number%Configuration.STORE_FILE_MAX_THREADS].join();
            }
            threadPool[current_chunk_number%Configuration.STORE_FILE_MAX_THREADS]=new Thread(new StoreQuerier(hashValue,current,this));
            threadPool[current_chunk_number%Configuration.STORE_FILE_MAX_THREADS].start();

            hashList.add(Base64.getEncoder().encodeToString(hashValue));

            //Read Next Chunk
            current = Utils.readBytesFromInputStream(input, Configuration.FILE_CHUNK_LENGTH_BYTES);
            current_chunk_number++;
        }

        for(int i=0;i<Configuration.STORE_FILE_MAX_THREADS;i++){
            if (threadPool[i]!=null)
                threadPool[i].join();
        }

        long finish = System.currentTimeMillis();

        System.out.println(Configuration.KADEMLIA_LOG+" Store lasted "+(finish-init)+"ms");

        return hashList;
    }


    /**<pre>
     * This method uses the list of keys outputted from a Node.store method and attempts to gather all the chunks of the data stored on kademlia
     * In order to improve download speeds to FVAL requests are parallelized using Configuration.RETRIEVE_FILE_MAX_THREADS number of threads
     * Since the data size can be larger than the amount of system memory available to kademlia, the data is stored on mass storage  Configuration.TEMP_PATH path
     * The amount of time that this method took to complete is outputted into the console in a text containing "...Retrieve data lasted..."
     * </pre>
     * @param keyList A list of keys that resemble the chunks uploaded into kademlia, each key is in Base64
     * @return A RetrieveResult object containing a input stream to the retrieved data and a String containing the file name that contains the data inside Configuration.TEMP_PATH
     * @throws InterruptedException
     * @throws ExecutionException
     * @throws IOException
     */

    public RetrieveResult retrieve(List<String> keyList) throws InterruptedException, ExecutionException, IOException {

        long initTimestamp = System.currentTimeMillis();


        //Create a temporary file with a random file name
        String temporaryFileName = RandomStringUtils.random(Configuration.TEMPORARY_FILE_NAME_LENGTH, true, true);
        File temporaryFile = new File(Configuration.TEMP_PATH+temporaryFileName);
        while (!temporaryFile.createNewFile()){
            temporaryFileName = RandomStringUtils.random(Configuration.TEMPORARY_FILE_NAME_LENGTH, true, true);
            temporaryFile = new File(Configuration.TEMP_PATH+temporaryFileName);
        }

        //Prepare a outputstream to write into the file
        FileOutputStream fos = new FileOutputStream(temporaryFile);

        Future<byte[]>[] downloadPool = new Future[Configuration.RETRIEVE_FILE_MAX_THREADS];
        ExecutorService pool = Executors.newFixedThreadPool(Configuration.RETRIEVE_FILE_MAX_THREADS);

        int current_chunk_number=0;

        String[] keyListArray = new String[keyList.size()];
        keyListArray = keyList.toArray(keyListArray);


        //Launching 10 Threads pre-fetching data
        for(;current_chunk_number<Math.min(keyListArray.length,Configuration.RETRIEVE_FILE_MAX_THREADS);current_chunk_number++){
            NodeID keyNodeID = new NodeID(Base64.getDecoder().decode(keyListArray[current_chunk_number]));
            downloadPool[current_chunk_number] = pool.submit(new RetrieveQuerier(keyNodeID,this));
        }



        //Gather the remaining keys data
        for(;current_chunk_number<keyListArray.length;current_chunk_number++){


            if (downloadPool[current_chunk_number%Configuration.RETRIEVE_FILE_MAX_THREADS]!=null){
                while (!downloadPool[current_chunk_number%Configuration.RETRIEVE_FILE_MAX_THREADS].isDone());

                byte[] partBytes = downloadPool[current_chunk_number % Configuration.RETRIEVE_FILE_MAX_THREADS].get();
                downloadPool[current_chunk_number % Configuration.RETRIEVE_FILE_MAX_THREADS]=null;
                fos.write(partBytes);

            }

            //Base64 Decode the next key to fetch
            NodeID keyNodeID = new NodeID(Base64.getDecoder().decode(keyListArray[current_chunk_number]));

            //Fetch the next key data
            downloadPool[current_chunk_number%Configuration.RETRIEVE_FILE_MAX_THREADS] = pool.submit(new RetrieveQuerier(keyNodeID,this));


        }

        //Recovering the remainder of the information on the worker threads
        int recoverInitIndex = current_chunk_number%Configuration.RETRIEVE_FILE_MAX_THREADS;

        for(int i = recoverInitIndex;i<Configuration.RETRIEVE_FILE_MAX_THREADS;i++){
            if (downloadPool[i]!=null) {
                byte[] partBytes = downloadPool[i].get();
                downloadPool[i] = null;
                fos.write(partBytes);
            }
        }

        for(int i=0;i<recoverInitIndex;i++){
            if (downloadPool[i]!=null) {
                byte[] partBytes = downloadPool[i].get();
                downloadPool[i] = null;
                fos.write(partBytes);
            }
        }

        fos.close();

        long FinishTimestamp = System.currentTimeMillis();

        System.out.println(Configuration.KADEMLIA_LOG+" Retrieve data lasted "+(FinishTimestamp-initTimestamp)+"ms");
        System.out.println(Configuration.BOOTSTRAPPER_LOG+" Stored at "+Configuration.TEMP_PATH+temporaryFileName);

        return new RetrieveResult(temporaryFileName, new FileInputStream(Configuration.TEMP_PATH+temporaryFileName));
    }


     public static class RetrieveResult{
        public String temporaryFileName;
        public InputStream inputStream;

        public RetrieveResult(String temporaryFileName, InputStream inputStream) {
            this.temporaryFileName = temporaryFileName;
            this.inputStream = inputStream;
        }
    }

    static class RetrieveQuerier implements Callable<byte[]>{
        NodeID itemID;
        Node source;

        public RetrieveQuerier(NodeID itemID, Node source) {
            this.itemID = itemID;
            this.source = source;
        }

        @Override
        public byte[] call() {
            try {
                return FindValue.query(itemID, source);
            }catch (Exception e){
                e.printStackTrace();
                return null;
            }
        }
    }

    static class PeriodicFNOD implements Runnable{

        Node node;

        public PeriodicFNOD(Node node) {
            this.node = node;
        }

        @Override
        public void run() {
            SecureRandom sc = new SecureRandom();
            while(true) {
                try {
                    System.out.println(Configuration.KADEMLIA_LOG + " Performing periodic FNOD");
                    FindNode.query(node.unique_id, node);
                    long sleep = sc.nextInt((Configuration.PERIODIC_FNOD_MAXIMUM_WAIT - Configuration.PERIODIC_FNOD_MINIMAL_WAIT) + 1) + Configuration.PERIODIC_FNOD_MINIMAL_WAIT;
                    System.out.println(Configuration.KADEMLIA_LOG + " Performing periodic FNOD after sleeping " + sleep + " seconds");
                    Thread.sleep(sleep * 1000);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    static class StoreQuerier implements Runnable{
        byte[] hashValue;
        byte[] content;
        Node source;

        public StoreQuerier(byte[] hashValue, byte[] content, Node source) {
            this.hashValue = hashValue;
            this.content = content;
            this.source = source;
        }

        @Override
        public void run() {
            try {
                Store.query(hashValue,content,source);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    class PeriodicReuploader implements Runnable {
        Node node;
        public PeriodicReuploader(Node node) {
            this.node = node;
        }

        @Override
        public void run() {
            Jedis jedis = new Jedis("localhost");


            while(true) {
                Set<String> keys = jedis.keys("*");
                SecureRandom sr = new SecureRandom();
                for (String k : keys) {
                    if (Files.exists(Paths.get(Configuration.STORE_PATH + k))) {
                        try {
                            FileInputStream fis = new FileInputStream(Configuration.STORE_PATH + k);
                            k = k.replace("_", "/");
                            Store.query(Base64.getDecoder().decode(k), Utils.readBytesFromInputStream(fis, Configuration.FILE_CHUNK_LENGTH_BYTES), node);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
                try {
                    int wait = sr.nextInt(Configuration.PERIODIC_REUPLOADER_MAXIMUM_WAIT - Configuration.PERIODIC_REUPLOADER_MINIMAL_WAIT+1)+Configuration.PERIODIC_REUPLOADER_MINIMAL_WAIT;
                    Thread.sleep(wait*1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
