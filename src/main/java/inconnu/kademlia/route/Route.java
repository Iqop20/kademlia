package inconnu.kademlia.route;

import java.io.Serializable;
import java.net.InetAddress;

/**<pre>
 * Route class stores information related to the ip address, port, nodeID and timestamp that are stored on the routing table
 * Also overrides comparison,equals and toString methods
 * </pre>
 */
public class Route implements Comparable<Route>, Serializable {
    public InetAddress ipAddress;
    public int port;
    public NodeID id;
    long timestamp;
    public boolean allowStore;
    public boolean isBootstrapper;

    public Route(InetAddress ipAddress, int port, NodeID id, long timestamp,boolean allowStore) {
        this.ipAddress = ipAddress;
        this.id = id;
        this.port = port;
        this.timestamp = timestamp;
        this.allowStore=allowStore;
        this.isBootstrapper = false;
    }

    public Route(InetAddress ipAddress, int port, NodeID id, long timestamp,boolean allowStore,boolean isBootstrapper) {
        this.ipAddress = ipAddress;
        this.id = id;
        this.port = port;
        this.timestamp = timestamp;
        this.allowStore=allowStore;
        this.isBootstrapper = false;
        this.isBootstrapper = isBootstrapper;
    }

    @Override
    public int compareTo(Route o) {
        if (timestamp==0 || o.timestamp==0){
            return id.compareTo(o.id);
        }
        if (timestamp!=o.timestamp) {
            return Long.compare(timestamp, o.timestamp);
        }else return id.compareTo(o.id);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj.getClass().equals(this.getClass())){
            return ((Route) obj).id.equals(this.id);
        }else return false;
    }

    /**
     * Sets the timestamp of this route at zero
     */
    public void eraseTimeStamp(){
        timestamp =0;
    }

    @Override
    public String toString() {
        return id.toString()+"::"+timestamp+"@"+ipAddress.toString()+":"+port;
    }
}
