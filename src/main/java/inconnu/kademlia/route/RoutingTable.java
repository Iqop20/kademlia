package inconnu.kademlia.route;

import inconnu.kademlia.Configuration;
import inconnu.kademlia.Node;
import inconnu.kademlia.commands.Ping;

import java.net.InetAddress;
import java.util.*;
import java.util.concurrent.Semaphore;
public class RoutingTable {
    Map<NodeID, TreeSet<Route>> routingTable;
    NodeID ownerID;
    Node owner;
    Semaphore semaphore;

    /**<pre>
     * Generates a routing table, the routing table is composed of K-Buckets that are constructed based on the owner nodeID
     * Routing table is generated in the following way (4bit node id example), the owner node id bits are 0011
     * |_       -> |1        ->  1 0 0 0
     *  0|_     ->  0|1      ->  0 1 0 0
     *  0 0|_   ->  0 0|0    ->  0 0 0 0
     *  0 0 1|_ ->  0 0 1|0  ->  0 0 1 0
     *
     *  For each line on the routing table dispose the node id bits, line 0 gets 0 bits,line 1 gets the first most significant bit, line 2 gets the two most significant bits and so on until reaching the last line of the routing table
     *  Then the for each line put the opposing bit of the node ID, the line 0 receives the opposite bit of the first most significant bit, line 1 received the opposite bit of the second most significant bit and so on until reaching the last line of the routing table
     *  Lastly fill the K-Buckets identifiers with zeros until each K-Bucket identifier has the same number of bits as the node id
     *
     *  Instead of 4 bits the nodeId in this implementation has 256 bits
     *  Each K-Bucket has a maximum number of routes that it can store (K)
     *  </pre>
     * @param owner The routing table owner
     */
    public RoutingTable(Node owner) {
        this.ownerID = owner.unique_id;
        this.owner = owner;
        routingTable = new HashMap<>();
        //this semaphore allows to control the concurrent routing table reads/writes that a multithreaded kademlia server might suffer by only allowing one thread to access/edit the data at a time
        semaphore = new Semaphore(1);


        //Generate KBuckets identifiers
        for (int i = Configuration.NODE_ID_LENGTH_BITS - 1; i >= 0; i--) {
            BitSet bs = new BitSet(Configuration.NODE_ID_LENGTH_BITS);
            for (int j = Configuration.NODE_ID_LENGTH_BITS - 1; j > i; j--) {
                if (ownerID.testBit(j)) {
                    bs.set(j);
                }
            }
            if (!ownerID.testBit(i)) {
                bs.set(i);
            }

            routingTable.put(new NodeID(bs.toByteArray()),new TreeSet<>());
        }

    }

    /**<pre>
     * This method stores a route into the routing table correct K-Bucket
     * If the route is already on the k-bucket, it will do nothing
     * If the K-Bucket is full it will check if some route on the K-Bucket has become unreachable by performing pings, if some has become unreachable it will be deleted and the new route will replace it
     * </pre>
     * @param address The ip address of the route to add
     * @param port The kademlia port of the route to add
     * @param id The nodeId of the route to add
     * @throws Exception
     */
    public void insert(InetAddress address,int port,boolean allowStore,NodeID id) throws Exception {
        //Safeguard to not store in the routing table a reference to the current node itself
        if (!id.equals(ownerID)) {

            //Get the correct K-Bucket identifier
            NodeID routingTableIndex = findRoutingTableIndex(id);


            semaphore.acquire();

            try {

                //Get the routes on the correct k-bucket
                TreeSet<Route> routes = routingTable.get(routingTableIndex);

                /*
                    Search for the id on the k-bucket routes, if there is a reference to the id it will finish without changing the routing table
                 */
                Route alreadyPresentRoute = null;
                for (Route r : routes) {
                    if (r.id.equals(id)) {
                        alreadyPresentRoute = r;
                    }
                }

                if (alreadyPresentRoute != null) {
//                    routes.remove(alreadyPresentRoute);
//                    routes.add(new Route(alreadyPresentRoute.ipAddress, alreadyPresentRoute.port, alreadyPresentRoute.id, System.currentTimeMillis()));
                    semaphore.release();
                    return;
                }


                /*
                    If the node is not on the routing table, start by checking if the k-bucket is full
                    If it is not full, insert the route immediately
                    Otherwise check if some route is unreachable and replace it with the new route
                 */
                if (routes.size() < Configuration.K) {
                    routes.add(new Route(address, port, id, System.currentTimeMillis(),allowStore));
                } else {


                    Route routeToBeRemoved = null;

                    for (Route r : routes) {
                        if (!Ping.query(r, owner)) {
                            routeToBeRemoved = r;
                            break;
                        }
                    }

                    if (routeToBeRemoved != null) {
                        if (!routeToBeRemoved.isBootstrapper) {
                            routes.remove(routeToBeRemoved);
                            routes.add(new Route(address, port, id, System.currentTimeMillis(), allowStore));
                        }
                    }

                }
                semaphore.release();
            } catch (Exception e) {
                e.printStackTrace();
                semaphore.release();
            }
        }

    }

    /**<pre>
     * This method stores a route into the routing table correct K-Bucket
     * If the route is already on the k-bucket, it will do nothing
     * If the K-Bucket is full it will check if some route on the K-Bucket has become unreachable by performing pings, if some has become unreachable it will be deleted and the new route will replace it
     * </pre>
     * @param address The ip address of the route to add
     * @param port The kademlia port of the route to add
     * @param id The nodeId of the route to add
     * @throws Exception
     */
    public void insert(InetAddress address,int port,boolean allowStore,NodeID id,boolean isBootstrapper) throws Exception {
        //Safeguard to not store in the routing table a reference to the current node itself
        if (!id.equals(ownerID)) {

            //Get the correct K-Bucket identifier
            NodeID routingTableIndex = findRoutingTableIndex(id);


            semaphore.acquire();

            try {

                //Get the routes on the correct k-bucket
                TreeSet<Route> routes = routingTable.get(routingTableIndex);

                /*
                    Search for the id on the k-bucket routes, if there is a reference to the id it will finish without changing the routing table
                 */
                Route alreadyPresentRoute = null;
                for (Route r : routes) {
                    if (r.id.equals(id)) {
                        alreadyPresentRoute = r;
                    }
                }

                if (alreadyPresentRoute != null) {
//                    routes.remove(alreadyPresentRoute);
//                    routes.add(new Route(alreadyPresentRoute.ipAddress, alreadyPresentRoute.port, alreadyPresentRoute.id, System.currentTimeMillis()));
                    semaphore.release();
                    return;
                }


                /*
                    If the node is not on the routing table, start by checking if the k-bucket is full
                    If it is not full, insert the route immediately
                    Otherwise check if some route is unreachable and replace it with the new route
                 */
                if (routes.size() < Configuration.K) {
                    routes.add(new Route(address, port, id, System.currentTimeMillis(),allowStore,isBootstrapper));
                } else {


                    Route routeToBeRemoved = null;

                    for (Route r : routes) {
                        if (!Ping.query(r, owner)) {
                            routeToBeRemoved = r;
                            break;
                        }
                    }

                    if (routeToBeRemoved != null) {
                        if (!routeToBeRemoved.isBootstrapper) {
                            routes.remove(routeToBeRemoved);
                            routes.add(new Route(address, port, id, System.currentTimeMillis(), allowStore, isBootstrapper));
                        }
                    }

                }
                semaphore.release();
            } catch (Exception e) {
                e.printStackTrace();
                semaphore.release();
            }
        }

    }


    /**<pre>
     * This method will search the routing table, starting by the correct K-Bucket where id belongs and will try to fetch n reachable routes from it
     * If the k-bucket has no necessary information to fulfill n routes, the method will try to fetch from other k-buckets that are XOR closer to the id in a descending order (start by the closest and finish on the farthest)
     * If n routes are collected or the entirety of the routing table is searched the method will return the routes found (number routes <= n)
     * </pre>
     * @param id The id that will base the search using XOR metric
     * @param n The number of nodes to fetch
     * @return The number of routes collected
     * @throws Exception
     */
    public List<Route> getNClosestNodes(NodeID id,int n) throws Exception {

        LinkedList<Route> toReturn = new LinkedList<>();

        //Get the correct K-Bucket for the id
        NodeID routingTableIndex = findRoutingTableIndex(id);

        semaphore.acquire();

        try {

            //Get the routes on the k-bucket
            TreeSet<Route> routes = routingTable.get(routingTableIndex);

            LinkedList<Route> toRemove = new LinkedList<>();

            /*
                Add all the reachable routes to the list that are reachable, if they are unreachable remove them from the routing table
                If the number of nodes is reached return it,
             */
            for (Route r : routes) {
                if (Ping.query(r, owner)) {
                    toReturn.add(r);
                } else {
                    toRemove.add(r);
                }
            }

            /*
                Remove unreachable routes from the k-bucket
             */
            for (Route r : toRemove) {
                if (!r.isBootstrapper) {
                    routes.remove(r);
                }
                toRemove.remove(r);
            }

            /*
                Return if the number of routes were reached
             */
            if (toReturn.size() >= n){
                semaphore.release();
                return  toReturn.subList(0,n-1);
            }


            /*
                Create a queue of k-bucket identifiers in the descending order (the closest to the node id first)
                Go through that queue until all the k-bucket were searched or n routes were found
             */
            Set<NodeID> nodeIDS = new HashSet<>(routingTable.keySet());
            nodeIDS.remove(routingTableIndex);

            Queue<NodeID> routingTableIndexQueue = sortByXOR(nodeIDS, id);
            boolean finished = false;

            while (toReturn.size() < n && routingTableIndexQueue.size() != 0 && !finished) {

                NodeID currentRoutingTableIndex = routingTableIndexQueue.remove();

                routes = routingTable.get(currentRoutingTableIndex);

                for (Route r : routes) {
                    if (Ping.query(r, owner)) {
                        toReturn.add(r);
                        if (toRemove.size() == n) {
                            finished = true;
                            break;
                        }
                    } else {
                        toRemove.add(r);

                    }
                }

                for (Route r : toRemove) {
                    if (!r.isBootstrapper) routes.remove(r);
                }

            }

            semaphore.release();
        }catch (Exception e){
            e.printStackTrace();
            semaphore.release();
        }

        return toReturn;
    }


    /**<pre>
     * This method will search the routing table, starting by the correct K-Bucket where id belongs and will try to fetch n reachable routes from it
     * If the k-bucket has no necessary information to fulfill n routes, the method will try to fetch from other k-buckets that are XOR closer to the id in a descending order (start by the closest and finish on the farthest)
     * If n routes are collected or the entirety of the routing table is searched the method will return the routes found (number routes <= n)
     *
     * THIS METHOD WILL ONLY RETURN NODES THAT CAN STORE INFORMATION
     * </pre>
     * @param id The id that will base the search using XOR metric
     * @param n The number of nodes to fetch
     * @return The number of routes collected that can store information
     * @throws Exception
     */
    public List<Route> getNClosestNodesWithStoreCapability(NodeID id,int n) throws Exception {

        LinkedList<Route> toReturn = new LinkedList<>();

        //Get the correct K-Bucket for the id
        NodeID routingTableIndex = findRoutingTableIndex(id);

        semaphore.acquire();

        try {

            //Get the routes on the k-bucket
            TreeSet<Route> routes = routingTable.get(routingTableIndex);

            LinkedList<Route> toRemove = new LinkedList<>();

            /*
                Add all the reachable routes to the list that are reachable, if they are unreachable remove them from the routing table
                If the number of nodes is reached return it,
             */
            for (Route r : routes) {
                if (r.allowStore) {
                    if (Ping.query(r, owner)) {
                        toReturn.add(r);
                    } else {
                        toRemove.add(r);
                    }
                }
            }

            /*
                Remove unreachable routes from the k-bucket
             */
            for (Route r : toRemove) {
                if (!r.isBootstrapper) {
                    routes.remove(r);
                }
                toRemove.remove(r);
            }

            /*
                Return if the number of routes were reached
             */
            if (toReturn.size() >= n){
                semaphore.release();
                return  toReturn.subList(0,n-1);
            }


            /*
                Create a queue of k-bucket identifiers in the descending order (the closest to the node id first)
                Go through that queue until all the k-bucket were searched or n routes were found
             */
            Set<NodeID> nodeIDS = new HashSet<>(routingTable.keySet());
            nodeIDS.remove(routingTableIndex);

            Queue<NodeID> routingTableIndexQueue = sortByXOR(nodeIDS, id);
            boolean finished = false;

            while (toReturn.size() < n && routingTableIndexQueue.size() != 0 && !finished) {

                NodeID currentRoutingTableIndex = routingTableIndexQueue.remove();

                routes = routingTable.get(currentRoutingTableIndex);

                for (Route r : routes) {
                    if (r.allowStore) {
                        if (Ping.query(r, owner)) {

                            toReturn.add(r);
                            if (toRemove.size() == n) {
                                finished = true;
                                break;
                            }
                        } else {
                            toRemove.add(r);
                        }
                    }
                }

                for (Route r : toRemove) {
                    if (!r.isBootstrapper) routes.remove(r);
                }

            }

            semaphore.release();
        }catch (Exception e){
            e.printStackTrace();
            semaphore.release();
        }

        return toReturn;
    }




    /** <pre>
     *  Sort a set of NodeIDs using XOR metric in relation to the currentKey, in descending order
     * </pre>
     * @param keySet The set to sort
     * @param currentKey The key that the XOR metric will be based on
     * @return The sorted set
     */
    private Queue<NodeID> sortByXOR(Set<NodeID> keySet, NodeID currentKey) {
        Map<NodeID,NodeID> map = new HashMap<>();

        for(NodeID id : keySet){
            map.put(currentKey.xor(id),id);
        }
        return new LinkedList<>((new TreeMap<>(map)).values());
    }


    /**<pre>
     * This method will match a node id with a k-bucket identifier
     * This method will clear the least significant bits until a match is found, the matched k-bucket will have the greater number of most significant bit equals to the node id
     * A match is always possible
     * </pre>
     * @param id The id to be matched
     * @return The K-bucket identifier that matches the id
     */
    private NodeID findRoutingTableIndex(NodeID id) {
        NodeID b_id = new NodeID(id.getNodeID());

        for (int i = 0; i <= Configuration.NODE_ID_LENGTH_BITS; i++) {

            if (routingTable.containsKey(b_id)){
                return b_id;
            }

            b_id = b_id.clearBit(i);
        }

        return null;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("--------------------\n\n");
        try {
            semaphore.acquire();
            routingTable.forEach((key,value) -> stringBuilder.append(key).append("-->[").append(printList(value)).append("]\n"));
            semaphore.release();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        stringBuilder.append("--------------------\n\n");
        return stringBuilder.toString();
    }

    private String printList(TreeSet<Route> routeSet){
        StringBuilder  stringBuilder = new StringBuilder();

        for(Route route : routeSet){
            stringBuilder.append(route.toString()).append(",\n");
        }
        return stringBuilder.toString();
    }
}
