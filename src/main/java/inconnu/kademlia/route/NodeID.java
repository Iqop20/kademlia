package inconnu.kademlia.route;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Base64;
import java.util.BitSet;

/**<pre>
 * NodeID class is a wrapper of the byte[] nodeId and it is responsible to override comparison,toString and equals methods
 * It also provides methods for XOR metric calculation including xor,clearBit and testBit
 * </pre>
 */
public class NodeID implements Comparable<NodeID>, Serializable {
    private byte[] nodeId;
    private BitSet bs;
    public NodeID(byte[] nodeId) {
        this.nodeId = nodeId;
        bs = BitSet.valueOf(this.nodeId);
    }


    public byte[] getNodeID(){
        return nodeId;
    }

    @Override
    public int compareTo(NodeID nodeID) {
        return Arrays.compare(nodeID.nodeId,this.nodeId);
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(this.nodeId);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj.getClass().equals(this.getClass())){
            return Arrays.equals(this.nodeId,((NodeID)obj).nodeId);
        }else return false;
    }


    /**
     * XORs both ids bit by bit
     * @param otherId The nodeID to xor
     * @return the nodeID containing the XOR result
     */
    public NodeID xor(NodeID otherId){

        BitSet bs2 = BitSet.valueOf(otherId.nodeId);
        bs2.xor(bs);

        return new NodeID(bs2.toByteArray());
    }

    /**
     * <pre>
     * Set the bit on the index of the nodeID at zero
     * </pre>
     * @param index Index of the bit to turn 0
     * @return A new nodeID with the index bit at zero
     */
    public NodeID clearBit(int index){
        BitSet bs = BitSet.valueOf(this.nodeId);
        bs.clear(index);
        return new NodeID(bs.toByteArray());
    }

    /**
     * Returns the value of the bit on the index as a boolean
     * @param index The index to get
     * @return true if bit=1, false otherwise
     */
    public boolean testBit(int index){
        return bs.get(index);
    }

    @Override
    public String toString() {
        return Base64.getEncoder().encodeToString(this.nodeId);
    }
}
