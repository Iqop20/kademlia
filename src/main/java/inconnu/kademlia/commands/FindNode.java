package inconnu.kademlia.commands;

import com.google.protobuf.ByteString;
import inconnu.kademlia.Configuration;
import inconnu.kademlia.Node;
import inconnu.kademlia.Utils;
import inconnu.kademlia.grpc.commands.FindNodeChunkedResponse;
import inconnu.kademlia.grpc.commands.FindNodeGrpc;
import inconnu.kademlia.grpc.commands.FindNodeRequest;
import inconnu.kademlia.grpc.commands.SignedMessage;
import inconnu.kademlia.route.NodeID;
import inconnu.kademlia.route.Route;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.stub.StreamObserver;

import java.io.*;

import java.net.InetAddress;
import java.security.*;
import java.security.spec.InvalidKeySpecException;
import java.util.*;
import java.util.concurrent.*;

public class FindNode {
    /**<pre>
     * This method receives a nodeID and attempts to perform a find node request based on that node
     * A findNode request gathers ALPHA nodes from the routing table and requests from them K-closest nodes from the destination nodeID on the argument
     * All the returned nodes will be collected into a array, then from that array ALPHA nodes are selected and the process repeats again
     * The process stops when the returned nodes from the previous iteration is the same as the returned nodes from the current iteration, meaning that there is no more
     * nodes to discover.
     * At the end the source node routing table will have all the minimal necessary nodes to reach all the nodes on the network either directly or indirectly.
     * There is no output, the routing table is updated as the process is completed
     * </pre>
     * @param destination The nodeID that will base the findNode requests
     * @param source The node that is sending the request (the current node). This reference is used to get the routingTable and other information from the node on demand
     * @throws Exception
     */
    public static void query(NodeID destination, Node source) throws Exception {

        System.out.println(Configuration.KADEMLIA_LOG+" Sending Find Node query with NodeID="+destination);

        //Set of nodes that the method already made a fnod query request
        Set<NodeID> alreadySelectedNodes = new HashSet<>();

        Set<NodeID> previousResult = new HashSet<>();

        Set<NodeID> currentResult = new HashSet<>();

        //Get ALPHA nodes from the routing table that are close to the destination
        Queue<Route> alphaClosestNodes = new LinkedList<>(source.routingTable.getNClosestNodes(destination, Configuration.ALPHA));

        boolean first=true;


        //The find node process only stops when the nodes collected from the previous iteration equals the nodes collected from the current result
        while(first || (!previousResult.equals(currentResult))){
            //Set the previous result as the current from the previous iteration and clear the previous result
            previousResult = new HashSet<>(currentResult);
            currentResult = new HashSet<>();
            first=false;


            //Since ALPHA is the concurrency parameter ALHA threads are generated, one for each if the ALPHA nodes collected
            ExecutorService pool = Executors.newFixedThreadPool(Configuration.ALPHA);
            Future<List<Route>>[] alphaFuture = new Future[Configuration.ALPHA];


            /*
             * Launch a thread for each ALPHA node in alphaClosestNodes
             * Insert the node into the routing table and mark it as already selected
             */
            for(int i=0;i<Configuration.ALPHA;i++){
                if (!alphaClosestNodes.isEmpty()) {
                    Route r = alphaClosestNodes.remove();
                        source.routingTable.insert(r.ipAddress,r.port,r.allowStore,r.id);
                        alphaFuture[i] =pool.submit(new FnodCallable(r, destination, source));
                        alreadySelectedNodes.add(r.id);
                }
            }


            /*
             * Wait for the ALPHA threads to complete and collect the output into a set
             */
            Set<Route> receivedRoutes = new HashSet<>();
            for(int i=0;i<Configuration.ALPHA;i++){
                if (alphaFuture[i]!=null){
                    while (!alphaFuture[i].isDone());

                    List<Route> fnodRes = alphaFuture[i].get();
                    if (fnodRes!=null) {
                        for (Route r : fnodRes) {
                            /*
                             * eraseTimeStamp marks the route timestamp as 0, preventing from having multiple routes of the same node with different timestamps,
                             * collected from different nodes
                             */
                            r.eraseTimeStamp();
                            receivedRoutes.add(r);
                        }
                    }
                }
            }


            /*
             * From all the collected routes from the previous step, ping the the routes to see if they are reachable
             * If the node was not already selected on a previous iteration and the route is reachable mark it as one of the ALPHA nodes for the next iteration
             */
            for (Route r : receivedRoutes) {
                if (alphaClosestNodes.size() < Configuration.ALPHA) {
                    if (!alreadySelectedNodes.contains(r.id)) {
                        if (Ping.query(r,source)) {
                            alphaClosestNodes.add(r);
                        }else alreadySelectedNodes.add(r.id);
                    }
                }
                //Add all the received routes into the current result in order to enable to check if the current received routes match the previous received routes
                currentResult.add(r.id);
            }
        }
    }


    /**<pre>
     * Class used to instantiate a worker thread that will request a node for KClosest Nodes close to the NodeID id
     * </pre>
     */
    static class FnodCallable implements Callable<List<Route>>{

        Route selectedRoute;
        NodeID id;
        Node source;
        public FnodCallable(Route selectedRoute,NodeID id, Node source) {
            this.selectedRoute = selectedRoute;
            this.id = id;
            this.source = source;
        }

        @Override
        public List<Route> call() throws Exception {

            //Generate a GRPC channel
            ManagedChannel channel = ManagedChannelBuilder.forAddress(selectedRoute.ipAddress.getHostAddress(),selectedRoute.port)
                    .usePlaintext()
                    .maxInboundMessageSize(Configuration.FILE_CHUNK_LENGTH_BYTES)
                    .build();

            //Generate a findNode stub to be able to perform the fnod rpc on the generated channel
            FindNodeGrpc.FindNodeStub findNodeStub = FindNodeGrpc.newStub(channel);


            //Convert the id into a byte array in order to ease the sending of information to the selectedRoute
            byte[] request = id.getNodeID();



            //Encapsulate the request message into a SignedMessage
            SignedMessage signedMessage = SignedMessage.newBuilder()
                    .setMessage(ByteString.copyFrom(request))
                    .setMessageSignature(ByteString.copyFrom(Utils.signMessageRSAWithSHA256(request,source.privateKey)))
                    .setSenderPublicKeyBytes(ByteString.copyFrom(source.publicKey.getEncoded()))
                    .setSignedPublicKeyBytes(ByteString.copyFrom(source.signedKey))
                    .build();



            final ByteArrayOutputStream[] bos = new ByteArrayOutputStream[1];

            final LinkedList<Route> routes = new LinkedList<>();


            //Necessary to make the method wait for the selectedRoute complete response
            CountDownLatch countDownLatch = new CountDownLatch(1);

            /*
             * Send the request and process the response
             * Since the response is received in chunks we need to be able to receive the chunks and aggregate them
             * The chunk aggregated result is the message sent by the selectedRoute and the signature received as the last chunk
             * matches the entirety of the aggregated chunk result
             * At the end all the received routes are in the routes LinkedList
             */
            findNodeStub.findNode(FindNodeRequest.newBuilder()
                    .setFindNodeSignedRequest(signedMessage)
                    .setAddress(source.ipAddress)
                    .setPort(source.port)
                    .setStore(source.allowStore)
                    .build(), new StreamObserver<FindNodeChunkedResponse>() {
                @Override
                public void onNext(FindNodeChunkedResponse findNodeResponse) {
                    switch (findNodeResponse.getResponseCase().getNumber()){
                        case 1: //messageChunk
                            try {
                                bos[0].write(findNodeResponse.getMessageChunk().toByteArray());
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                            break;
                        case 2: //completeSignature

                            byte[] message = bos[0].toByteArray();

                            SignedMessage signedMessage = findNodeResponse.getCompleteMessageSignature();

                            try {
                                if (Utils.verifySignedMessageSignatures(signedMessage,message,source.nodeBootStraperPublicKey)){
                                    routes.addAll((LinkedList<Route>) Utils.convertByteArrayToObject(message));
                                }
                            } catch (NoSuchAlgorithmException | InvalidKeySpecException | SignatureException | InvalidKeyException | IOException | ClassNotFoundException e) {
                                e.printStackTrace();
                            }


                            break;
                        case 3: //messageLength
                            int messageAndSignaturelength = findNodeResponse.getMessagelength();
                            bos[0] = new ByteArrayOutputStream(messageAndSignaturelength);
                            break;
                    }
                }

                @Override
                public void onError(Throwable throwable) {

                    throwable.printStackTrace();
                    //count down the latch to signal end of transmission by the server
                    countDownLatch.countDown();
                    routes.clear();
                }

                @Override
                public void onCompleted() {
                    //count down the latch to signal end of transmission by the server
                    countDownLatch.countDown();
                }
            });

            //Lock until the countDownLatch.countDown() is called
            countDownLatch.await();

            //Shutdown the channel to release resources
            channel.shutdownNow();
            while(!channel.awaitTermination(1,TimeUnit.MINUTES)){
                channel.shutdownNow();
            }


            /*
             * Return the received routes if some route was received, returns null otherwise
             */
            if (routes.size()!=0){
                return routes;
            }
            return null;
        }
    }


    /**
     * <pre>
     * This class extends the FindNodeGrpc.FindNodeImplBase and implements the methods that will be responsible to answer the fnod requests
     * This class is then instantiated into a Grpc Server
     * </pre>
     */
    public static class FindNodeService extends FindNodeGrpc.FindNodeImplBase{
        Node responder;

        public FindNodeService(Node responder){
            this.responder=responder;
        }

        /**<pre>
         * This method is responsible to answer find node requests
         * </pre>
         * @param request The find node request
         * @param responseObserver A StreamObserver that enables to answer the request received
         */
        @Override
        public void findNode(FindNodeRequest request, StreamObserver<FindNodeChunkedResponse> responseObserver) {
            System.out.println(Configuration.KADEMLIA_LOG+" Received FNOD request");

            //Get the SignedMessage on the request
            SignedMessage findNodeSignedRequest = request.getFindNodeSignedRequest();

            try {
                //Verify request message signatures
                if (Utils.verifySignedMessageSignatures(findNodeSignedRequest,responder.nodeBootStraperPublicKey)){

                    /*
                        Get the KClosest Nodes that are close to the requested id
                        Serialize the KClosest Nodes list into a byte array
                        Sign the byte array contents
                     */
                    byte[] response = Utils.convertObjectToByteArray(responder.routingTable.getNClosestNodes(new NodeID(findNodeSignedRequest.getMessage().toByteArray()), Configuration.K));
                    byte[] responseSignature = Utils.signMessageRSAWithSHA256(response,responder.privateKey);

                    //Prepared the signed response
                    SignedMessage signedMessage = SignedMessage.newBuilder()
                            .setMessageSignature(ByteString.copyFrom(responseSignature))
                            .setSenderPublicKeyBytes(ByteString.copyFrom(responder.publicKey.getEncoded()))
                            .setSignedPublicKeyBytes(ByteString.copyFrom(responder.signedKey))
                            .build();



                    //Send the signed response chunk by chunk
                    //Start by sending the size of the complete response
                    responseObserver.onNext(FindNodeChunkedResponse.newBuilder().setMessagelength(response.length).build());

                    /*
                     * Read the response byte array chunk by chunk sending the read chunk to the requester
                     * The chunk length is always <=Configuration.GRPC_MAX_LENGTH_BYTES
                     */
                    ByteArrayInputStream bis = new ByteArrayInputStream(response);
                    byte[] read = Utils.readBytesFromInputStream(bis,Configuration.GRPC_MAX_LENGTH_BYTES);
                    while (read.length!=0){
                        responseObserver.onNext(FindNodeChunkedResponse.newBuilder().setMessageChunk(ByteString.copyFrom(read)).build());
                        read = Utils.readBytesFromInputStream(bis,Configuration.GRPC_MAX_LENGTH_BYTES);
                    }

                    //Lastly send the message signature to make the verification possible on the requester side
                    responseObserver.onNext(FindNodeChunkedResponse.newBuilder().setCompleteMessageSignature(signedMessage).build());
                    //Mark the transmission as completed
                    responseObserver.onCompleted();

                    //Add the requester node into the routing table
                    try {
                        responder.routingTable.insert(InetAddress.getByName(request.getAddress()),request.getPort(),request.getStore(),new NodeID(Utils.hashSHA256(findNodeSignedRequest.getSenderPublicKeyBytes().toByteArray())));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }else{
                    //Send the Signature Verification Failed error to the requester node
                    responseObserver.onError(new Exception("Signature Verification Failed"));
                }
            } catch (NoSuchAlgorithmException | InvalidKeySpecException | SignatureException | InvalidKeyException e) {
                e.printStackTrace();
                //If some error occurs send the error and mark the transmission as completed
                responseObserver.onError(e);
            } catch (Exception e) {
                e.printStackTrace();
            }


        }
    }
}
