package inconnu.kademlia.commands;

import com.google.protobuf.ByteString;
import inconnu.kademlia.Configuration;
import inconnu.kademlia.Node;
import inconnu.kademlia.Utils;
import inconnu.kademlia.grpc.commands.FindValueChunkedResponse;
import inconnu.kademlia.grpc.commands.FindValueGrpc;
import inconnu.kademlia.grpc.commands.FindValueRequest;
import inconnu.kademlia.grpc.commands.SignedMessage;
import inconnu.kademlia.route.NodeID;
import inconnu.kademlia.route.Route;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.stub.StreamObserver;
import redis.clients.jedis.Jedis;

import java.io.*;
import java.net.InetAddress;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.SignatureException;
import java.security.spec.InvalidKeySpecException;
import java.util.*;
import java.util.concurrent.*;

public class FindValue {

    /**<pre>
     * This method is responsible to find the value associated to the key itemID
     * The method works very similarly to the inconnu.kademlia.commands.FindNode.query, the only difference is that
     * the responses to the requests can be a findnode response (list of KClosest routes) or the contents of the value associated to the key itemID
     * In order to reduce local resource consumption the findValue does not spawn ALPHA worker threads
     * </pre>
     * @param itemID The key associated to the value that we want to get
     * @param source The node that is sending the request (the current node). This reference is used to get the routingTable and other information from the node on demand
     * @return The value associated to the key itemID or null otherwise
     * @throws Exception
     */
    public static byte[] query(NodeID itemID, Node source) throws Exception {


        System.out.println(Configuration.KADEMLIA_LOG+" Sending Find Value query with key="+itemID);
        Set<NodeID> alreadySelectedNodes = new HashSet<>();

        Set<NodeID> previousResult = new HashSet<>();

        Set<NodeID> currentResult = new HashSet<>();

        List<Route> alphaClosestNodesList = source.routingTable.getNClosestNodes(itemID, Configuration.ALPHA);
        /*
            Shuffle the alphaClosestNodes to reduce the possibility of only requesting data from one of the nodes that possesses the value,
            reducing the stress on a specific node
         */
        Collections.shuffle(alphaClosestNodesList,new SecureRandom());

        Queue<Route> alphaClosestNodes = new LinkedList<>(alphaClosestNodesList);

        boolean first=true;

        //The find node process only stops when the nodes collected from the previous iteration equals the nodes collected from the current result
        while(first || (!previousResult.equals(currentResult))){
            //Set the previous result as the current from the previous iteration and clear the previous result
            previousResult = new HashSet<>(currentResult);
            currentResult = new HashSet<>();
            first=false;



            Set<Route> receivedRoutes = new HashSet<>();

            /*
             * Send a FNOD request to each one of the ALPHA nodes selected
             * Add the node into the routing table
             * Wait for the response, it can be a null value, a list of routing tables or the value associated to the key itemID
             */
            for(int i=0;i<Configuration.ALPHA;i++){
                if (!alphaClosestNodes.isEmpty()) {
                    Route r = alphaClosestNodes.remove();

                    source.routingTable.insert(r.ipAddress,r.port,r.allowStore,r.id);

                    Result fnodRes= processor(r, itemID, source);

                    if (fnodRes.foundFile){
                        System.out.println("!!!!"+fnodRes.contents.length);
                        return fnodRes.contents;
                    }else {
                        for (Route s : fnodRes.routes) {
                            s.eraseTimeStamp();
                            receivedRoutes.add(s);
                        }
                    }
                    alreadySelectedNodes.add(r.id);
                }
            }

            /*
             * From all the collected routes from the previous step, ping the the routes to see if they are reachable
             * If the node was not already selected on a previous iteration and the route is reachable mark it as one of the ALPHA nodes for the next iteration
             */
            for (Route r : receivedRoutes) {
                if (alphaClosestNodes.size() < Configuration.ALPHA) {
                    if (!alreadySelectedNodes.contains(r.id)) {
                        if (Ping.query(r,source)) {
                            alphaClosestNodes.add(r);
                        }else alreadySelectedNodes.add(r.id);
                    }
                }
                //Add all the received routes into the current result in order to enable to check if the current received routes match the previous received routes
                currentResult.add(r.id);
            }
        }

        //Returns null if the findvalue method fails to get the value associated to the key
        return null;
    }


    /**
     * <pre>
     * A class responsible to store the processor method output
     * </pre>
     */
    static class Result{
        boolean foundFile;
        List<Route> routes;
        byte[] contents;

        public Result(){contents=null;routes=null;}

    }


    static Result processor(Route r,NodeID key,Node source) throws NoSuchAlgorithmException, InvalidKeyException, SignatureException, InterruptedException {

        //Generate a GRPC channel
        ManagedChannel channel = ManagedChannelBuilder.forAddress(r.ipAddress.getHostAddress(), r.port)
                .usePlaintext()
                .maxInboundMessageSize(Configuration.FILE_CHUNK_LENGTH_BYTES)
                .build();

        //Generate a findValue stub to be able to perform the fval rpc on the generated channel
        FindValueGrpc.FindValueStub findValueStub = FindValueGrpc.newStub(channel);


        //Encapsulate the request message into a SignedMessage
        SignedMessage signedRequest = SignedMessage.newBuilder()
                .setMessage(ByteString.copyFrom(key.getNodeID()))
                .setMessageSignature(ByteString.copyFrom(Utils.signMessageRSAWithSHA256(key.getNodeID(), source.privateKey)))
                .setSenderPublicKeyBytes(ByteString.copyFrom(source.publicKey.getEncoded()))
                .setSignedPublicKeyBytes(ByteString.copyFrom(source.signedKey))
                .build();


        final ByteArrayOutputStream[] bos = new ByteArrayOutputStream[1];

        final Result result = new Result();

        //Necessary to make the method wait for the selectedRoute complete response
        CountDownLatch countDownLatch = new CountDownLatch(1);

        /*
         * Send the request and process the response
         * Since the response is received in chunks we need to be able to receive the chunks and aggregate them
         * The chunk aggregated result is the message sent by the selectedRoute and the signature received as the last chunk
         * matches the entirety of the aggregated chunk result
         * At the end all the received routes are in the routes LinkedList
         */
        findValueStub.findValue(FindValueRequest.newBuilder()
                .setFindValueSignedRequest(signedRequest)
                .setAddress(source.ipAddress)
                .setPort(source.port)
                .setStore(source.allowStore)
                .build(), new StreamObserver<FindValueChunkedResponse>() {

            @Override
            public void onNext(FindValueChunkedResponse findValueChunkedResponse) {
                switch (findValueChunkedResponse.getResponseCase().getNumber()) {

                    case 2: //messageLength
                        int messageLength = findValueChunkedResponse.getMessageLength();
                        bos[0] = new ByteArrayOutputStream(messageLength);

                        break;
                    case 3: //messageChunk
                        byte[] bytes = findValueChunkedResponse.getMessageChunk().toByteArray();
                        try {
                            bos[0].write(bytes);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        break;
                    case 4: //signature

                        byte[] message = bos[0].toByteArray();

                        SignedMessage completeMessageSignature = findValueChunkedResponse.getCompleteMessageSignature();

                        try {
                            if (Utils.verifySignedMessageSignatures(completeMessageSignature, message, source.nodeBootStraperPublicKey)) {
                                if (findValueChunkedResponse.getHasValue()) {
                                    //Has value
                                    result.foundFile = true;
                                    System.out.println("!!!!"+message.length);
                                    result.contents = message;
                                } else {
                                    //Has Kclosest
                                    result.foundFile = false;
                                    try {
                                        result.routes = (List<Route>) Utils.convertByteArrayToObject(message);
                                    } catch (IOException | ClassNotFoundException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                        } catch (NoSuchAlgorithmException | InvalidKeySpecException | SignatureException | InvalidKeyException e) {
                            e.printStackTrace();

                        }
                }
            }

            @Override
            public void onError(Throwable throwable) {
                throwable.printStackTrace();
                //count down the latch to signal end of transmission by the server
                countDownLatch.countDown();
            }

            @Override
            public void onCompleted() {
                //count down the latch to signal end of transmission by the server
                countDownLatch.countDown();
            }
        });


        //Lock until the countDownLatch.countDown() is called
        countDownLatch.await();


        //Shutdown the channel to release resources
        channel.shutdownNow();
        while(!channel.awaitTermination(1,TimeUnit.MINUTES)){
            channel.shutdownNow();
        }

        //Return the gathered result
        return result;
    }


    /**
     * <pre>
     * This class extends the FindValueGrpc.FindValueImplBase and implements the methods that will be responsible to answer the fval requests
     * This class is then instantiated into a Grpc Server
     * </pre>
     */
    public static class FindValueService extends FindValueGrpc.FindValueImplBase{
        Node responder;
        public FindValueService(Node responder){
            this.responder = responder;
        }

        /**<pre>
         * This method is responsible to answer find value requests
         * </pre>
         * @param request The find value request
         * @param responseObserver A StreamObserver that enables to answer the request received
         */
        @Override
        public void findValue(FindValueRequest request, StreamObserver<FindValueChunkedResponse> responseObserver) {
            System.out.println(Configuration.KADEMLIA_LOG+" Received FVAL request");

            //Get the SignedMessage on the request
            SignedMessage findValueSignedRequest = request.getFindValueSignedRequest();
            try {
                //Verify request message signatures
                if (Utils.verifySignedMessageSignatures(findValueSignedRequest,responder.nodeBootStraperPublicKey)){


                    //Fix the received key into one that is storage friendly
                    String fName = Base64.getEncoder().encodeToString(findValueSignedRequest.getMessage().toByteArray());
                    fName = fName.replace("/", "_");

                    boolean found=false;


                    //Connect to the redis database that stores a list of the stored keys
                    Jedis storeDatabase = new Jedis("localhost");

                    //If the key exists on the database then get the contents from the node STORE_PATH
                    if (storeDatabase.exists(fName)) {

                            /*
                                Get the value and send it chunked
                             */
                        if (Files.exists(Paths.get(Configuration.STORE_PATH + fName))) {

                            //Send the value length
                            responseObserver.onNext(FindValueChunkedResponse.newBuilder().setHasValue(true).setMessageLength(Configuration.GRPC_MAX_LENGTH_BYTES).build());

                            found = true;

                            //Opens a FileInputStream to read from the file and send the chunks to the requester nodes
                            FileInputStream fis = new FileInputStream(Configuration.STORE_PATH+fName);
                            byte[] read = Utils.readBytesFromInputStream(fis, Configuration.GRPC_MAX_LENGTH_BYTES);
                            while (read.length != 0) {
                                //Send message chunk
                                responseObserver.onNext(FindValueChunkedResponse.newBuilder()
                                        .setHasValue(true)
                                        .setMessageChunk(ByteString.copyFrom(read))
                                        .build());
                                read = Utils.readBytesFromInputStream(fis, Configuration.GRPC_MAX_LENGTH_BYTES);
                            }

                            //Sign the file contents
                            byte[] file = Utils.readBytesFromInputStream(new FileInputStream(Configuration.STORE_PATH+fName),(int) new File(Configuration.STORE_PATH+fName).length());
                            byte[] signature = Utils.signMessageRSAWithSHA256(file, responder.privateKey);


                            //Generate a SignedMessage
                            SignedMessage valueFoundSignedResponse = SignedMessage.newBuilder()
                                    .setMessageSignature(ByteString.copyFrom(signature))
                                    .setSenderPublicKeyBytes(ByteString.copyFrom(responder.publicKey.getEncoded()))
                                    .setSignedPublicKeyBytes(ByteString.copyFrom(responder.signedKey))
                                    .build();

                            //Lastly send the message signature to make the verification possible on the requester side
                            responseObserver.onNext(FindValueChunkedResponse.newBuilder()
                                    .setHasValue(true)
                                    .setCompleteMessageSignature(valueFoundSignedResponse)
                                    .build());

                        }
                    }

                    /*
                        Send the KClosest nodes chunk by chunk, similar to a findNode response
                     */
                    if (!found){
                        byte[] kClosestNodes = Utils.convertObjectToByteArray(responder.routingTable.getNClosestNodes(new NodeID(findValueSignedRequest.getMessage().toByteArray()), Configuration.K));


                        responseObserver.onNext(FindValueChunkedResponse.newBuilder().setHasValue(false).setMessageLength(kClosestNodes.length).build());


                        ByteArrayInputStream bis = new ByteArrayInputStream(kClosestNodes);
                        byte[] read = Utils.readBytesFromInputStream(bis,Configuration.GRPC_MAX_LENGTH_BYTES);
                        while (read.length!=0){
                            responseObserver.onNext(FindValueChunkedResponse.newBuilder()
                                    .setHasValue(false)
                                    .setMessageChunk(ByteString.copyFrom(read))
                                    .build());
                            read = Utils.readBytesFromInputStream(bis,Configuration.GRPC_MAX_LENGTH_BYTES);
                        }

                        SignedMessage signedMessage = SignedMessage.newBuilder()
                                .setMessageSignature(ByteString.copyFrom(Utils.signMessageRSAWithSHA256(kClosestNodes,responder.privateKey)))
                                .setSenderPublicKeyBytes(ByteString.copyFrom(responder.publicKey.getEncoded()))
                                .setSignedPublicKeyBytes(ByteString.copyFrom(responder.signedKey))
                                .build();

                        responseObserver.onNext(FindValueChunkedResponse.newBuilder()
                                .setHasValue(false)
                                .setCompleteMessageSignature(signedMessage)
                                .build());

                    }

                    //Mark the transmission as complete
                    responseObserver.onCompleted();

                    //Add the requester node into the routing table
                    try {
                        responder.routingTable.insert(InetAddress.getByName(request.getAddress()),request.getPort(),request.getStore(),new NodeID(Utils.hashSHA256(findValueSignedRequest.getSenderPublicKeyBytes().toByteArray())));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }else responseObserver.onError(new Exception("Signature Verification failed"));
            } catch (Exception e) {
                e.printStackTrace();
                responseObserver.onError(e);
            }


        }
    }
}
