package inconnu.kademlia.commands;

import com.google.protobuf.ByteString;
import inconnu.kademlia.Configuration;
import inconnu.kademlia.Node;
import inconnu.kademlia.Utils;
import inconnu.kademlia.grpc.commands.SignedMessage;
import inconnu.kademlia.grpc.commands.StoreChunkedRequest;
import inconnu.kademlia.grpc.commands.StoreGrpc;
import inconnu.kademlia.grpc.commands.StoreResponse;
import inconnu.kademlia.route.NodeID;
import inconnu.kademlia.route.Route;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.stub.StreamObserver;
import org.apache.commons.lang3.RandomStringUtils;
import redis.clients.jedis.Jedis;

import java.io.*;
import java.net.InetAddress;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import java.security.spec.InvalidKeySpecException;
import java.util.Base64;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

public class Store {

    /**<pre>
     * This method is responsible to store a (key,data) pair into kademlia, the key will be used to gather the data back using FindValue rpc
     * The store rpc sends requests to at least K closest nodes in relation to the key using XOR metric enabling redundancy
     * and ensuring data availability
     * </pre>
     * @param key The key of the (key,data) pair
     * @param data The data byte array of the (key,data) pair to be stored
     * @param source The node that is sending the request (the current node).
     * @throws Exception
     */
    public static void query(byte[] key,byte[] data,Node source) throws Exception {

        System.out.println(Configuration.KADEMLIA_LOG+" Sending Store query of data with key="+Base64.getEncoder().encodeToString(key));

        //Gather the K closest nodes in relation ot the key using XOR metric that allows storage
        List<Route> kClosestNodes = source.routingTable.getNClosestNodesWithStoreCapability(new NodeID(key), Configuration.K);

        /*
         * Convert the (key,data) into a object and serialize it
         * Sign the serialized result
         */
        KeyValuePair keyValuePair = new KeyValuePair(key,data);
        byte[] keyValuePairBytes = Utils.convertObjectToByteArray(keyValuePair);
        byte[] signature = Utils.signMessageRSAWithSHA256(keyValuePairBytes,source.privateKey);


        /*
         *
         */
        for(Route r: kClosestNodes){
            if (Ping.query(r,source)){

                //Generate a GRPC channel
                ManagedChannel channel = ManagedChannelBuilder.forAddress(r.ipAddress.getHostAddress(),r.port)
                        .usePlaintext()
                        .maxInboundMessageSize(Configuration.FILE_CHUNK_LENGTH_BYTES)
                        .build();

                //Generate a store stub to be able to perform the store rpc on the generated channel
                StoreGrpc.StoreStub storeStub = StoreGrpc.newStub(channel);

                //CountDownLatch will be used to lock the method until a response from the server is received
                CountDownLatch countDownLatch = new CountDownLatch(1);

                /*
                 * If the node that we are going to send a store request does not support storing, it will send a error message
                 * This flag allows the system to stop
                 */
                AtomicBoolean stopSending=new AtomicBoolean(false);

                StreamObserver<StoreChunkedRequest> store = storeStub.store(new StreamObserver<StoreResponse>() {
                    /*
                     * For each chunk received and processed by the receiving node it returns the randomID that represents the chunk received
                     */
                    @Override
                    public void onNext(StoreResponse storeResponse) {
                        SignedMessage storeSignedResponse = storeResponse.getStoreSignedResponse();
                        try {
                            if (Utils.verifySignedMessageSignatures(storeSignedResponse, source.nodeBootStraperPublicKey)) {
                                System.out.println(new String(storeSignedResponse.getMessage().toByteArray()));
                            }
                        } catch (NoSuchAlgorithmException | InvalidKeySpecException | SignatureException | InvalidKeyException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(Throwable throwable) {
                        if (throwable.getMessage().equals("This node does not support store")) {
                            System.out.println(Configuration.KADEMLIA_LOG+" "+throwable.getMessage());
                        }else{
                            throwable.printStackTrace();
                        }
                        //count down the latch to signal end of transmission by the server
                        countDownLatch.countDown();
                        stopSending.set(true);
                    }

                    @Override
                    public void onCompleted() {
                        //count down the latch to signal end of transmission by the server
                        countDownLatch.countDown();
                        stopSending.set(true);
                    }
                });

                /*
                 * Read the response byte array chunk by chunk sending the read chunk to the requester
                 * The chunk length is always <=Configuration.GRPC_MAX_LENGTH_BYTES
                 */
                ByteArrayInputStream bis = new ByteArrayInputStream(keyValuePairBytes);
                byte[] read = Utils.readBytesFromInputStream(bis,Configuration.GRPC_MAX_LENGTH_BYTES);
                while(read.length!=0 && !stopSending.get()){
                    //randomId to tag the chunk sent
                    String random = RandomStringUtils.random(20, true, true);

                    //Send Chunk
                    store.onNext(StoreChunkedRequest.newBuilder()
                            .setChunkId(random)
                            .setMessageChunk(ByteString.copyFrom(read))
                            .setAddress(source.ipAddress)
                            .setPort(source.port)
                            .setStore(source.allowStore)
                            .build());

                    read = Utils.readBytesFromInputStream(bis,Configuration.GRPC_MAX_LENGTH_BYTES);
                }

                if(!stopSending.get()) {
                    //Prepare SignedMessage containing the complete signature of the data sent
                    SignedMessage signedMessage = SignedMessage.newBuilder()
                            .setMessageSignature(ByteString.copyFrom(signature))
                            .setSenderPublicKeyBytes(ByteString.copyFrom(source.publicKey.getEncoded()))
                            .setSignedPublicKeyBytes(ByteString.copyFrom(source.signedKey))
                            .build();


                    //Send the SignedMessage
                    String random = RandomStringUtils.random(20, true, true);
                    store.onNext(StoreChunkedRequest.newBuilder()
                            .setChunkId(random)
                            .setCompleteMessageSignature(signedMessage)
                            .setAddress(source.ipAddress)
                            .setPort(source.port)
                            .setStore(source.allowStore)
                            .build());


                    //Mark the transmission as complete
                    store.onCompleted();

                }

                //Lock until the countDownLatch.countDown() is called
                countDownLatch.await();

                //Shutdown the channel to release resources
                channel.shutdownNow();
                while(!channel.awaitTermination(1, TimeUnit.MINUTES)){
                    channel.shutdownNow();
                }

            }
        }
    }

    public static class KeyValuePair implements Serializable {
        public byte[] key;
        public byte[] value;

        public KeyValuePair(byte[] key, byte[] value) {
            this.key = key;
            this.value = value;
        }
    }

    public static class StoreService extends StoreGrpc.StoreImplBase{
        Node responder;
        public StoreService(Node responder){
            this.responder = responder;
        }

        /**<pre>
         * This method is responsible to answer store requests
         * </pre>
         * @param responseObserver A StreamObserver that enables to answer the request received
         * @return A StreamObserver capable of receiving chunked data
         */
        @Override
        public StreamObserver<StoreChunkedRequest> store(StreamObserver<StoreResponse> responseObserver) {
            System.out.println(Configuration.KADEMLIA_LOG+" Received STOR request");
            return new Processor(responseObserver);
        }


        /**
         * This class implements a StreamObserver<StoreChunkedRequest> and will be responsible to answer the store requests
         */
        class Processor implements StreamObserver<StoreChunkedRequest>{
            StreamObserver<StoreResponse> responseObserver;
            ByteArrayOutputStream bos;
            public Processor(StreamObserver<StoreResponse> responseObserver) {
                this.responseObserver = responseObserver;
                //reserve space only if this node supports storage
                if (responder.allowStore) {
                    this.bos = new ByteArrayOutputStream(Configuration.FILE_CHUNK_LENGTH_BYTES);
                }
            }

            /**
             * For each store chunk received this method is called, it will receive, verify signature and store data into the local storage
             * The data will be stored into a ByteArrayOutputStream prior to storing
             * @param storeChunkedRequest The chunk received
             */
            @Override
            public void onNext(StoreChunkedRequest storeChunkedRequest) {
                String chunkId = storeChunkedRequest.getChunkId();
                switch (storeChunkedRequest.getResponseCase().getNumber()){
                    case 2: //messageChunk
                        try {
                            //write only if this node supports store
                            if (responder.allowStore) {
                                bos.write(storeChunkedRequest.getMessageChunk().toByteArray());
                                //Send Confirmation
                                SignedMessage signedMessage = SignedMessage.newBuilder()
                                        .setMessage(ByteString.copyFrom(chunkId.getBytes()))
                                        .setMessageSignature(ByteString.copyFrom(Utils.signMessageRSAWithSHA256(chunkId.getBytes(),responder.privateKey)))
                                        .setSenderPublicKeyBytes(ByteString.copyFrom(responder.publicKey.getEncoded()))
                                        .setSignedPublicKeyBytes(ByteString.copyFrom(responder.signedKey))
                                        .build();
                                responseObserver.onNext(StoreResponse.newBuilder().setStoreSignedResponse(signedMessage).build());

                            }else{
                                responseObserver.onError(new Exception("This node does not support store"));
                            }


                        } catch (IOException | NoSuchAlgorithmException | SignatureException | InvalidKeyException e) {
                            e.printStackTrace();
                        }
                        break;
                    case 3: //completeMessageSignature

                        /*
                         * Attempts to store the key and value on the local storage only if storage is enabled on this node
                         * Otherwise does nothing
                         */
                        if (responder.allowStore) {

                            //Get the SignedMessage on the request
                            SignedMessage completeMessageSignature = storeChunkedRequest.getCompleteMessageSignature();

                            byte[] message = bos.toByteArray();
                            try {
                                //Verify request message signatures
                                if (Utils.verifySignedMessageSignatures(completeMessageSignature, message, responder.nodeBootStraperPublicKey)) {

                                    //Get the keyValuePair serialized object from the request
                                    KeyValuePair keyValuePair = (KeyValuePair) Utils.convertByteArrayToObject(message);

                                    //Fix the received key into one that is storage friendly
                                    String fName = Base64.getEncoder().encodeToString(keyValuePair.key);
                                    fName = fName.replace("/", "_");


                                    //Connect to the redis database that stores a list of the stored keys
                                    Jedis storeDatabase = new Jedis("localhost");

                                    /*
                                        Check if the key already exists, if not adds the entry to the redis database and writes data into local storage STORE_PATH
                                        If the key exists the expiring date is renewed
                                     */
                                    if (!storeDatabase.exists(fName)) {
                                        storeDatabase.set(fName, fName);
                                        storeDatabase.expire(fName, Configuration.STORE_ENTRY_EXPIRICY);

                                        File f = new File(Configuration.STORE_PATH + fName);
                                        if (f.createNewFile()) {
                                            FileOutputStream fos = new FileOutputStream(f);
                                            fos.write(keyValuePair.value);
                                            fos.close();
                                        } else System.out.println("File " + fName + " already exists");
                                    } else {
                                        storeDatabase.expire(fName, Configuration.STORE_ENTRY_EXPIRICY);
                                    }


                                    //Sends a message to the requester signaling it is done processing
                                    byte[] finalResponse = "done".getBytes(StandardCharsets.UTF_8);
                                    byte[] finalResponseSignature = Utils.signMessageRSAWithSHA256(finalResponse, responder.privateKey);

                                    SignedMessage signedMessage = SignedMessage.newBuilder()
                                            .setMessage(ByteString.copyFrom(finalResponse))
                                            .setMessageSignature(ByteString.copyFrom(finalResponseSignature))
                                            .setSenderPublicKeyBytes(ByteString.copyFrom(responder.publicKey.getEncoded()))
                                            .setSignedPublicKeyBytes(ByteString.copyFrom(responder.signedKey))
                                            .build();

                                    responseObserver.onNext(StoreResponse.newBuilder().setStoreSignedResponse(signedMessage).build());

                                    //Marks the transmission as complete
                                    responseObserver.onCompleted();

                                    //Add the requester node into the routing table
                                    try {
                                        responder.routingTable.insert(InetAddress.getByName(storeChunkedRequest.getAddress()), storeChunkedRequest.getPort(),storeChunkedRequest.getStore(),new NodeID(Utils.hashSHA256(completeMessageSignature.getSenderPublicKeyBytes().toByteArray())));
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                } else throw new Exception("Signature Verification Failed");
                            } catch (Exception e) {
                                e.printStackTrace();
                                responseObserver.onError(e);
                            }
                        }else{

                            responseObserver.onError(new Exception("This node does not support store"));
                        }
                        break;
                }
            }

            @Override
            public void onError(Throwable throwable) {
                throwable.printStackTrace();
            }

            @Override
            public void onCompleted() {
            }
        }
    }

}
