package inconnu.kademlia.commands;

import com.google.protobuf.ByteString;
import inconnu.kademlia.Configuration;
import inconnu.kademlia.Node;
import inconnu.kademlia.Utils;
import inconnu.kademlia.grpc.commands.*;
import inconnu.kademlia.route.NodeID;
import inconnu.kademlia.route.Route;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.stub.StreamObserver;
import org.apache.commons.lang3.RandomStringUtils;

import java.net.InetAddress;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import java.security.spec.InvalidKeySpecException;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;

public class Ping {

    /**<pre>
     * This method is responsible to ping the node represented by the destination route
     * Ping works be sending a random string to the destination node and wait for the ping result that will a message with
     * the same as the random string sent
     * </pre>
     * @param destination The destination route that will received the ping
     * @param source The node that is sending the request (the current node). This reference is used to get the private key and other information from the node on demand
     * @return The ping result, true if the ping was successful and false otherwise
     * @throws Exception
     */
    public static boolean query(Route destination,Node source) throws Exception {
        System.out.println(Configuration.KADEMLIA_LOG+" Sending a Ping to node "+destination);

        //Generate a GRPC channel
        ManagedChannel channel = ManagedChannelBuilder.forAddress(destination.ipAddress.getHostAddress(),destination.port)
                .usePlaintext()
                .maxInboundMessageSize(Configuration.FILE_CHUNK_LENGTH_BYTES)
                .build();

        //Generate a ping blocking stub to be able to perform the ping rpc on the generated channel
        PingGrpc.PingBlockingStub pingBlockingStub = PingGrpc.newBlockingStub(channel);

        //Generate the ping message content
        byte[] ping = RandomStringUtils.random(64, true, true).getBytes();

        //Encapsulate the ping message into a SignedMessage
        SignedMessage signedMessage = SignedMessage.newBuilder()
                .setMessage(ByteString.copyFrom(ping))
                .setMessageSignature(ByteString.copyFrom(Utils.signMessageRSAWithSHA256(ping,source.privateKey)))
                .setSenderPublicKeyBytes(ByteString.copyFrom(source.publicKey.getEncoded()))
                .setSignedPublicKeyBytes(ByteString.copyFrom(source.signedKey))
                .build();

        PingResponse pingResponse=null;
        boolean failed=false;
        boolean returnResult=false;
        try {
            //Send the ping query to the destination an hope for a result
            pingResponse = pingBlockingStub.ping(PingRequest.newBuilder()
                    .setPingSignedRequest(signedMessage)
                    .setAddress(source.ipAddress)
                    .setPort(source.port)
                    .setStore(source.allowStore)
                    .build());

        }catch (Exception e){
            //If the connection fails for some reason the failed flag is raised
            failed=true;
        }

        //When the connection was successful and a response arrived the next steps will check if the received message has the excepted signature and expected content
        if (!failed) {
            SignedMessage pingSignedResponse = pingResponse.getPingSignedResponse();

            if (Utils.verifySignedMessageSignatures(pingSignedResponse, source.nodeBootStraperPublicKey)) {
                returnResult = Arrays.equals(pingSignedResponse.getMessage().toByteArray(), ping);
            }
        }

        //Shutdown the channel to release resources
        channel.shutdownNow();
        while(!channel.awaitTermination(1, TimeUnit.MINUTES)){
            channel.shutdownNow();
        }

        /*
         * Returns the ping result
         * true if the message was received and the contents are those expected and false otherwise
         */
        return  returnResult;
    }


    /**<pre>
     * This class extends the PingGrpc.PingImplBase and implements the methods that will be responsible to answer the ping requests
     * This class is then instantiated into a Grpc Server
     * </pre>
     */
    public static class PingService extends PingGrpc.PingImplBase{

        Node responder;

        public PingService(Node responder){
            this.responder = responder;
        }

        /**<pre>
         * This method is responsible to answer ping requests
         *</pre>
         * @param request The ping request received
         * @param responseObserver A StreamObserver that enables to answer the request received
         */
        @Override
        public void ping(PingRequest request, StreamObserver<PingResponse> responseObserver) {
            System.out.println(Configuration.KADEMLIA_LOG+" Received PING request");

            //Get the SignedMessage on the request
            SignedMessage pingSignedRequest = request.getPingSignedRequest();

            try {
                //Verify request message signatures
                if (Utils.verifySignedMessageSignatures(pingSignedRequest,responder.nodeBootStraperPublicKey)){

                    //Build a response
                    SignedMessage response = SignedMessage.newBuilder()
                            .setMessage(pingSignedRequest.getMessage())
                            .setMessageSignature(ByteString.copyFrom(Utils.signMessageRSAWithSHA256(pingSignedRequest.getMessage().toByteArray(),responder.privateKey)))
                            .setSenderPublicKeyBytes(ByteString.copyFrom(responder.publicKey.getEncoded()))
                            .setSignedPublicKeyBytes(ByteString.copyFrom(responder.signedKey))
                            .build();


                    //Send the response
                    responseObserver.onNext(PingResponse.newBuilder()
                            .setPingSignedResponse(response)
                            .build());

                    //Mark the response as finished
                    responseObserver.onCompleted();

                }
            } catch (NoSuchAlgorithmException | InvalidKeySpecException | SignatureException | InvalidKeyException e) {
                e.printStackTrace();
                //Send the error to the receiver party
                responseObserver.onError(e);
            }

            try {
                //Add the node that made the request into the routing table, in order to make the information on the routing table accommodate the nodes on the routing table
                responder.routingTable.insert(InetAddress.getByName(request.getAddress()),request.getPort(),request.getStore(),new NodeID(Utils.hashSHA256(pingSignedRequest.getSenderPublicKeyBytes().toByteArray())));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
