package inconnu.kademlia;

import inconnu.kademlia.commands.FindNode;
import inconnu.kademlia.route.NodeID;

import java.net.InetAddress;
import java.util.Base64;

public class NormalNode extends Node{
    /**Launches a kademlia node
     * @param bootStrapIP The ip of the bootstrapping node
     * @param port The port where the kademlia component will be listening to
     * @param localOrExternalIp "local" or "external", sets the ip address to use on kademlia
     * @param store "true" or "false" and means if this node is capable of storing data
     * @throws Exception
     */
    public NormalNode(String bootStrapIP,int port,String localOrExternalIp,String store,String mongoAddress) throws Exception {
        super(port,localOrExternalIp,store,mongoAddress);
        System.out.println(Configuration.KADEMLIA_LOG+" initializing node");
        
        String b64PublicKey = "MIICIjANBgkqhkiG9w0BAQEFAAOCAg8AMIICCgKCAgEAx1b06IQKk+AwZ1z4b1TNkcn4ZYOXRFaLIt4qx2zbKSXjGpkVANbL3ISb2bqXmOxqz0yC+kkkFGdhg6QFR4Xfn8TAu/oMTb3jQwWdHd3LSALYLltEcuJggBa5xg2Ybwor3gLywf2Wl2IlI5rfeKxoLALKDhVyIB8/2h2YrpZ0l/1dlGgk214qhz4W+kZlL/DlDtXhWlmySeP9IX/6iXEpQ3QK2mX+MCzeAXryE5vhUjjb41Nitr8YQnKL+uE+Lwos1bTGMSJKMgis46soeyGQVvQaNyNo+y1iOB0jBCqVuHlYc174BnXpggYQEYmQs/dmyDQMIpnyfq1z1IX2y7oJ4uibSeFDbHfA7/hADE5Z8cixruARWeg4yXb44OCAyWGVTs+hep2HQUrFaAONAjQEs3ZVnoHoUPOBotxvqPASVKaE7haLMbBEyZ0OHH4twg5S6Z8cixvjO3pfL/M4evGN/E+wxJNnXlEk3L2EicLFqBWDZOPu7n4yN7BjaSpGnqtKo5ToJ2yGUYeNTQtNcoCJHYrsrFwc4ylpMQoR1XcEGwLgazpx5sCyiHMN6JhSazDfelYVTVi4VpFeiWBes0O+RatX0GF8vfSrUzkbR3kPQ/yZtgJBoHLIGUiD4UlIK8a/EKe+KQfgTyh3vVJGFYuPGffZMofE671izJzTZ/NWoh0CAwEAAQ==";

        nodeBootStraperPublicKey = Base64.getDecoder().decode(b64PublicKey);

        //Signs the public key of the node on the bootstrapping server
        signKey(InetAddress.getByName(bootStrapIP));

        //Initialize the routing table
        initRoutingTable();
        
        System.out.println(Configuration.KADEMLIA_LOG+" Inserting bootstrapper node into the routing table");
        routingTable.insert(InetAddress.getByName(bootStrapIP),Configuration.BOOTSTRAPPER_KADEMLIA_PORT,false,new NodeID(Base64.getDecoder().decode("qxorFA/kGc2EC0MFEA59XlEZPnRzrcWQZjZ/XRqz6uI=")),true);
        //Launch the kademlia server that will listen on the argument port
        launchKademliaServer();

        Thread.sleep(1000);


        //Gather information about the current nodes from the BootStrapper node, after this the bootstrapper node will no longer be necessary
        FindNode.query(this.unique_id,this);

        /*
            Initialize periodic FNODs that are responsible to keep the routing table updated
         */
        initPeriodicFNODS();
        if (store.equals("true")) initPeriodicReuploader();

    }

}
