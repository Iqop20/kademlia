package inconnu.kademlia;

public class NormalNodeInit {
    public static void main(String[] args) throws Exception {
        if (args.length!=5){
            System.out.println("Needed four arguments, check README.md");
            return;
        }

        new NormalNode(args[0], Integer.parseInt(args[1]), args[2], args[3],args[4]);
    }
}
