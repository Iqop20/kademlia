package inconnu.kademlia;

public class Configuration {
    public static final int BOOTSTRAPPER_BOOTSTRAP_PORT =8100;
    public static final int BOOTSTRAPPER_KADEMLIA_PORT =8200;

    public static final int K=20;
    public static final int ALPHA=3;

    public static final int SIGN_POW_LENGTH_BITS=20;
    public static final int NODE_ID_LENGTH_BITS=256;

    public static final int FILE_CHUNK_LENGTH_BYTES = 8*1024*1024;

    public static final int GRPC_MAX_LENGTH_BYTES = FILE_CHUNK_LENGTH_BYTES-2*1024;

    public static final int STORE_FILE_MAX_THREADS = 10;

    public static final int RETRIEVE_FILE_MAX_THREADS = 10;

    public static final int TEMPORARY_FILE_NAME_LENGTH=100;

    public static final String KADEMLIA_LOG = "### KADEMLIA LOG: ";
    public static final String BOOTSTRAPPER_LOG = "### BOOTSTRAPPER LOG: ";

    public static final int PERIODIC_FNOD_MINIMAL_WAIT = 20;
    public static final int PERIODIC_FNOD_MAXIMUM_WAIT = 120;

    public static final int STORE_ENTRY_EXPIRICY = 10*60; //20Minutes

    public static final int PERIODIC_REUPLOADER_MINIMAL_WAIT = STORE_ENTRY_EXPIRICY + 60;
    public static final int PERIODIC_REUPLOADER_MAXIMUM_WAIT = STORE_ENTRY_EXPIRICY + 120;

    public static final String STORE_PATH="/mnt/store/";
    public static final String TEMP_PATH="/mnt/tmp/";







}
